# TODO:
## Important:
* Setup lights.
* Implement PowerUps.
* Implement competitors collision.
* Implement HUD.

## Bugs:
* Track description interpreter only reads vectors when there is a space(' ') after the vector coordinates.

## Optional:
* Implement the menu (kind of important).
* The verifyCheckpoints method in the Track class can be easily optimized (probably making the code less readable).
* Make the AI better and generic.