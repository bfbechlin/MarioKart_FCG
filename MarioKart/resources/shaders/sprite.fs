#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D image;
uniform vec3 spriteColor;

void main()
{   
	if(spriteColor == vec3(-1.0, -1.0, -1.0)){
		vec4 texColor = texture(image, TexCoords);
		if(texColor.a < 0.9)
			discard;
		color = texColor;
	}
	else{
		color = vec4(spriteColor, 1.0) * texture(image, TexCoords);
	}
}  