#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoords;

out vec2 textureCoords;
out vec3 normalFrag;
out vec3 positionFrag;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
	positionFrag = vec3(model * vec4(position, 1.0f));
	normalFrag = vec3(model * vec4(normal, 1.0f));
    textureCoords = vec2(texCoords.x, 1.0 - texCoords.y);
}
