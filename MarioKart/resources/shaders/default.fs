#version 330 core

in vec2 textureCoords;
in vec3 normalFrag;
in vec3 positionFrag;

out vec4 color;
uniform vec3 viewPosition;

// Light
uniform vec3 position;
uniform vec3 ambient;
uniform vec3 diffuse;
uniform vec3 specular;

// Material
uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform float shininess;

void main()
{
	// Ambient
    vec3 ambient = ambient * vec3(texture(texture_diffuse1, textureCoords));
	
	// Diffuse
	vec3 norm = normalize(normalFrag);
	vec3 lightDirection = normalize(position - positionFrag);
	float diff = max(dot(norm, lightDirection), 0.0);
	vec3 diffuse = diffuse * diff * vec3(texture(texture_diffuse1, textureCoords));
	
	// Specular
	vec3 viewDirection = normalize(viewPosition - positionFrag);
	vec3 reflectDirection = reflect(-lightDirection, norm);
	float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), shininess);
	vec3 specular = specular * spec * vec3(texture(texture_specular1, textureCoords));
	
	color = vec4(ambient + diffuse + specular, 1.0f);
}
