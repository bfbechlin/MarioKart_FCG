#include "boundingbox2d.h"

#include <stdio.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>

BoundingBox2D::BoundingBox2D(glm::vec3 center, const GLdouble w, const GLdouble h, glm::vec3 direction) :
    center(center)
{
    this->direction = glm::normalize(direction);
    // directionOrto = 90 degree rotated in the y axis direction
    glm::vec3 directionOrto(direction.z, direction.y, -direction.x);

    directionOrto *= w/2;
    direction *= h/2;
    // (-w/2, 0, -h/2); (w/2, 0, -h/2); (w/2, 0, h/2); (-w/2, 0, h/2); when direction is equal to (0, 0, 1);

    vertex[0] = center - direction - directionOrto;
    vertex[1] = center - direction + directionOrto;
    vertex[2] = center + direction + directionOrto;
    vertex[3] = center + direction - directionOrto;

    computeMatrixModel();
    computeAxis();
}

void BoundingBox2D::movement(glm::vec3 newPosition, glm::vec3 newDirection)
{
    // Set the new position for the center of bounding Box
    center = newPosition;
    // Set the direction to the normalized new direction
    direction = glm::normalize(newDirection);
    // Recalculate the vertices
    computeVertex();
}

void BoundingBox2D::copy(BoundingBox2D* source)
{
    // Copy the vertices
    for (size_t i = 0; i < sizeof(vertex)/sizeof(glm::vec3); i++)
        this->vertex[i] = source->vertex[i];
    // Copy the center and direction
    this->center = source->center;
    this->direction = source->direction;
    // Copy the axis
    for (size_t i = 0; i < sizeof(axis)/sizeof(glm::vec3); i++)
        this->axis[i] = source->axis[i];
    // Copy the origins
    for (size_t i = 0; i < sizeof(origin)/sizeof(GLdouble); i++)
        this->origin[i] = source->origin[i];
    // Copy the models
    for (size_t i = 0; i < sizeof(model)/sizeof(glm::mat4); i++)
        this->model[i] = source->model[i];
}

glm::vec3 BoundingBox2D::getCenter()
{
    return center;
}

glm::vec3 BoundingBox2D::getDirection()
{
    return direction;
}

void BoundingBox2D::printData()
{
    printf("Boundig Box Data\n");
    printf("-Center:    %f %f %f\n", center.x, center.y, center.z);
    printf("-Direction: %f %f %f\n", direction.x, direction.y, direction.z);
    printf("-Vertices positions:\n");
    for (size_t i = 0; i < sizeof(vertex)/sizeof(glm::vec3); i++)
        printf("\tVertex[%d]: %f %f %f\n", i, vertex[i].x, vertex[i].y, vertex[i].z);
    printf("-Axis:\n");
    for (size_t i = 0; i < sizeof(axis)/sizeof(glm::vec3); i++)
        printf("\tAxis[%d]: %f %f %f\n", i, axis[i].x, axis[i].y, axis[i].z);
    printf("-Origins:\n");
    for (size_t i = 0; i < sizeof(origin)/sizeof(GLdouble); i++)
        printf("\tOrigin[%d]: %lf\n", i, origin[i]);
    printf("-Models:\n");
    for (size_t i = 0; i < sizeof(model)/sizeof(glm::mat4); i++)
    {
        const float *matPtr = (const float*)glm::value_ptr(model[i]);
        printf("\tModel[%d]: \n", i);
        for (int j = 0; j < 4; j++)
            printf("\t%f\t%f\t%f\t%f\n", matPtr[4*j+0], matPtr[4*j+1], matPtr[4*j+2], matPtr[4*j+3]);
    }
}

bool BoundingBox2D::overlaps(BoundingBox2D* other)
{
    return overlaps1Way(other, false) && other->overlaps1Way(this, false);
}

bool BoundingBox2D::isInside(BoundingBox2D* other)
{
    return other->overlaps1Way(this, true);
}

bool BoundingBox2D::overlaps1Way(BoundingBox2D* other, bool inside)
{
    for (size_t i = 0; i < sizeof(axis)/sizeof(glm::vec3); i++)
    {
        GLdouble distance = glm::dot(other->vertex[0], axis[i]);

        GLdouble dMin = distance;
        GLdouble dMax = distance;

        // Calculate the closest and farthest points in this axis
        for (size_t j = 1; j < sizeof(vertex)/sizeof(glm::vec3); j++)
        {
            distance = glm::dot(other->vertex[j], axis[i]);

            if (distance < dMin)
                dMin = distance;
            else if (distance > dMax)
                dMax = distance;
        }

        if (((inside ? dMax : dMin) > origin[i] + 1) || ((inside ? dMin : dMax) < origin[i]))
        {
            // No intersection in this dimension, the boxes doesn't overlap
            return false;
        }
    }
    // Intersection occurred in all dimension, the boxes overlap
    return true;
}

void BoundingBox2D::computeAxis()
{
    axis[0] = vertex[1] - vertex[0];
    axis[1] = vertex[3] - vertex[0];

    for (size_t i = 0; i < sizeof(axis)/sizeof(glm::vec3); i++)
    {
        axis[i] /= glm::length2(axis[i]);
        origin[i] = glm::dot(vertex[0], axis[i]);
    }
}

void BoundingBox2D::computeVertex()
{
    // Create a matrix transform to translate a point in the origin to destination
    glm::mat4 translateMatrix = glm::translate(center);
    // Create a point with coordenates of the UNITY vector direction
    glm::vec4 orientation(direction, 1.0f);
    glm::vec4 vertices;
    for (size_t i = 0; i < sizeof(model)/sizeof(glm::mat4); i++){
        // Rotate the point to assume the vertex boundingBox
        vertices = model[i]*orientation;
        // Translate this point to localization of boundingBox
        vertices = translateMatrix*vertices;
        vertex[i] = glm::vec3(vertices);
    }
    // Compute new axis
    computeAxis();
}

void BoundingBox2D::computeMatrixModel()
{
    GLfloat scaleFactor, angleXZ;
    glm::vec3 auxVector;
    glm::mat4 matrixTransform;

    for(size_t i = 0; i < sizeof(model)/sizeof(glm::mat4); i++){
        // Getting this vector
        auxVector = vertex[i];
        // Getting the distance of point to center(0, 0, 0)
        scaleFactor = glm::length(auxVector);
        // Normalizing this vector
        auxVector = glm::normalize(auxVector);
        // Calculing the angle in the plane XZ between the direction vector initial(0, 0, 1) and the auxVector
        angleXZ = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), auxVector, glm::vec3(0.0f, 1.0f, 0.0f));
        // Creating a matrix model to transform a vector direction into a vexter independent of the orientation
        matrixTransform = glm::rotate(angleXZ, glm::vec3(0.0f, 1.0f, 0.0f))*glm::scale(glm::vec3(scaleFactor, scaleFactor, scaleFactor));
        model[i] = matrixTransform;
    }
}
