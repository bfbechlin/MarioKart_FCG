#include "ai.h"

#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include "constants.h"
#include "global.h"

AI::AI()
{
    srand(time(NULL));
    currentState = Z_PN;

    collided = false;
    collideTimer = 0.0;

    resetStatus();
}

void AI::process(Track* trackInfo, Competitor* competitorInfo)
{
#if AI_ACTIVE
    resetStatus();

    if (collided)
    {
        brakeAction = true;
        collideTimer -= Global::deltaTime();
        if (collideTimer <= 0.0)
            collided = false;
        return;
    }

    GLfloat angle = glm::orientedAngle(competitorInfo->updatedCine.direction, this->expectedDirection, glm::vec3(0.0, 1.0, 0.0));
    switch (currentState)
    {
    case Z_PN:
        expectedDirection = glm::vec3(0.0, 0.0, -1.0);
        if (competitorInfo->updatedCine.position.z < -32.0*trackInfo->scale)
            currentState = X_PN;
        if (competitorInfo->currentCine.position.x < 28.0*trackInfo->scale)
            currentState = X_NP;
        break;

    case X_PN:
        expectedDirection = glm::vec3(-1.0, 0.0, 0.0);
        if (competitorInfo->updatedCine.position.x < -18.0*trackInfo->scale)
            currentState = Z_NP;
        if (competitorInfo->currentCine.position.z > -32.0*trackInfo->scale)
            currentState = Z_PN;
        break;

    case Z_NP:
        expectedDirection = glm::vec3(0.0, 0.0, 1.0);
        if (competitorInfo->updatedCine.position.z > 14.0*trackInfo->scale)
            currentState = X_NP;
        if (competitorInfo->currentCine.position.x > -18.0*trackInfo->scale)
            currentState = X_PN;
        break;

    case X_NP:
        expectedDirection = glm::vec3(1.0, 0.0, 0.0);
        if (competitorInfo->updatedCine.position.x > 28.0*trackInfo->scale)
            currentState = Z_PN;
        if (competitorInfo->currentCine.position.z < 14.0*trackInfo->scale)
            currentState = Z_NP;
        break;
    }

    if (angle > glm::radians(100.0) || angle < glm::radians(-100.0))
        expectedSpeed = 10;
    else
        expectedSpeed = competitorInfo->kartStats.maxVelocity;

    if (competitorInfo->updatedCine.velocityModule < expectedSpeed)
        accelerateAction = true;
    else if (competitorInfo->updatedCine.velocityModule > expectedSpeed+5)
        brakeAction = true;

    if (angle > 0)
        leftAction = true;
    else if (angle < 0)
        rightAction = true;

    if (!competitorInfo->haveEmptySlot() && rand() % 100 == 0)
        useItemAction =  true;

    if (competitorInfo->updatedCine.velocityModule < 1.0)
    {
        collideTimer += Global::deltaTime();
        if (collideTimer > 1.0)
            collided = true;
    }
#endif // AI_ACTIVE
}

void AI::resetStatus()
{
    accelerateAction = false;
    brakeAction = false;
    leftAction = false;
    rightAction = false;
    useItemAction = false;
}
