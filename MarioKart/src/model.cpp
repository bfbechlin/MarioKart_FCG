#include "model.h"

#include <stdio.h>
#include <stdlib.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SOIL/SOIL.h>

#include "constants.h"

GLint TextureFromFile(const char* path, std::string directory);

Model::Model(const char* path, GLuint shaderProgram) :
    shaderProgram(shaderProgram)
{
    this->loadModel(path);
}

void Model::draw(glm::mat4 model, Camera* camera, Light* light)
{
    glUseProgram(shaderProgram);

    // Transformation matrices
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(camera->projection));
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(camera->view));
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

    // Draw the loaded model
    for (GLuint i = 0; i < meshes.size(); i++)
        meshes[i].draw(shaderProgram, camera, light);
}

void Model::loadModel(std::string path)
{
    this->directory = path.substr(0, path.find_last_of('/'));

    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;
    bool ret = tinyobj::LoadObj(shapes, materials, err, path.c_str(), (this->directory + '/').c_str());

    if (!err.empty()) {
        printf("%s\n", err.c_str());
    }

    if (!ret) {
        exit(1);
    }

    for (size_t i = 0; i < shapes.size(); i++)
    {
        this->meshes.push_back(this->processShape(shapes[i], materials));
    }
}

Mesh Model::processShape(tinyobj::shape_t shape, std::vector<tinyobj::material_t> materials)
{
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;
    std::vector<Texture> textures;

    // Process the vertices
    for (size_t i = 0; i < shape.mesh.positions.size() / 3; i++)
    {
        Vertex vertex;
        glm::vec3 vector3;

        // Process the position
        vector3.x = shape.mesh.positions[3*i+0];
        vector3.y = shape.mesh.positions[3*i+1];
        vector3.z = shape.mesh.positions[3*i+2];
        vertex.position = vector3;

        // Process the normals
        vector3.x = shape.mesh.normals[3*i+0];
        vector3.y = shape.mesh.normals[3*i+1];
        vector3.z = shape.mesh.normals[3*i+2];
        vertex.normal = vector3;

        // Process the textures
        if (shape.mesh.texcoords.size() > 0)
        {
            glm::vec2 vector2;
            vector2.x = shape.mesh.texcoords[2*i+0];
            vector2.y = shape.mesh.texcoords[2*i+1];
            vertex.textureCoords = vector2;
        }
        else
            vertex.textureCoords = glm::vec2(0.0f, 0.0f);

        vertices.push_back(vertex);
    }

    // Process the indices
    for (size_t i = 0; i < shape.mesh.indices.size(); i++)
    {
        indices.push_back(shape.mesh.indices[i]);
    }

    // Process the material
    for (size_t i = 0; i < shape.mesh.material_ids.size(); i++)
    {
        tinyobj::material_t material = materials[shape.mesh.material_ids[i]];

        if (!material.diffuse_texname.empty())
            textures.push_back(this->loadMaterialTexture(material.diffuse_texname, "texture_diffuse"));

        if (!material.specular_texname.empty())
            textures.push_back(this->loadMaterialTexture(material.specular_texname, "texture_specular"));
    }

    return Mesh(vertices, indices, textures);
}

Texture Model::loadMaterialTexture(std::string texturePath, std::string type)
{
    // If the texture was already loaded, return its ID
    for (size_t i = 0; i < texturesLoaded.size(); i++)
    {
        if (texturesLoaded[i].path == texturePath)
            return texturesLoaded[i];
    }

    // If it wasn't loaded yet, load it
    Texture texture;
    texture.id   = TextureFromFile(texturePath.c_str(), this->directory);
    texture.type = type;
    texture.path = texturePath;
    this->texturesLoaded.push_back(texture);

    return texture;
}

GLint TextureFromFile(const char* path, std::string directory)
{
    //Generate texture ID and load texture
    std::string filename = std::string(path);
    filename = directory + '/' + filename;

    GLuint texture;
    glGenTextures(1, &texture);

    int imageW, imageH, comp;
    unsigned char* image = SOIL_load_image(filename.c_str(), &imageW, &imageH, &comp, SOIL_LOAD_RGB);
    if(image == 0)
    {
        printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
    }

    // Assign texture to ID
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageW, imageH, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);

    // Parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    SOIL_free_image_data(image);
    return texture;
}
