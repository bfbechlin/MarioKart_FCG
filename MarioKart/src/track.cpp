#include "track.h"

#include <stdio.h>
#include <stdlib.h>
#include <fstream>

#include "constants.h"
#include "global.h"
#include "sprite.h"

/*
 * Track description(.trdesc) file:
 *
 * The .trdesc files describes data from a track - namely, the title of the track, the position of collectible boxes and the track model.
 *
 * ATTENTION: TABS ARE NOT CONSIDERED AS SPACES, USE ONLY SPACES AS SEPARATOR. AND IT ONLY WORKS WITH SPACES AFTER THE COORDINATES.
 *
 * The title can be specified with a line beginning with 't'.
 * If more than one line begin with 't' the first of them will specify the title.
 * Eg: t Simple Track
 *
 * The track model can be specified with a line beginning with 'm'.
 * The path is relative to the executable.
 * Use $MODELS_PATH to get the game's default models folder.
 * If more than one line begin with 'm' the last of them will specify the model.
 * Eg: m $MODELS_PATH simple_track/raceway_mariokart.obj
 *
 * Use 'mi' to specify the minimap for the track.
 * Eg: mi $MODELS_PATH simple_track/minimap.png
 *
 * With 's' you can specify the map scale. Must come before the other definitions.
 * Eg: s 10.0
 *
 * With 'la' you can specify the number of laps.
 * Eg: la 3
 *
 * Lines beginning with 'c' specify the competitors positions and direction relative to the original track scale.
 * The maximum number of competitors of a track will be the number of positions defined.
 * Also the maximum number of competitor will be limited by the constant MAX_COMPETITORS
 * Eg: c 10.0 10.0 0.0    0.0 0.0 -1.0
 *
 * Lines beginning with 'b' specify the collectible boxes positions.
 * Eg: b 10.0 10.0 0.0
 *
 * Lines beginning with 'ch' specify the checkpoints position, dimension and direction in order.
 * The first checkpoint will be the finish line.
 * Eg: ch 30.0 0.0 8.0    2.0 0.0 2.0    0.0 0.0 1.0
 *
 * Lines beginning with 'w' specify the limitations or "walls" of the track.
 * Competitors will collide with these limitations.
 * The order is position, dimension and direction.
 * Eg: w 5.0 0.0 -9.0   48.0 0.2 48.0   0.0 0.0 1.0
 *
 * Lines beginning with 'l' define light sources position and ambient, diffuse and specular color.
 * Eg: l 40.0 20.0 0.0   0.2 0.2 0.2   0.5 0.5 0.5   1.0 1.0 1.0
 *
 * Any line that doesn't begin with any of the above will be ignored. Use # before comments for optimization.
 */

glm::vec3 getVectorFromLine(std::string line, int* endPos = 0);

Track::Track(const char* path, GLuint shaderProgram)
{
    this->numLaps = 0;
    this->scale = 1.0;
    loadTrack(path, shaderProgram);
}

void Track::update()
{
    // Update boxes rotation (30 degree in y and z axis)
    Box::rotation = Box::rotation + glm::radians(30.0)*Global::deltaTime();
    // Update boxes status
    for (size_t i = 0; i < collectibles.size(); i++)
        collectibles[i]->update(Global::deltaTime());
}

void Track::render(Camera* camera)
{
    // Render the track
    glm::mat4 modelMatrix;
    modelMatrix = glm::scale(modelMatrix, glm::vec3(scale, scale, scale));
    this->model->draw(modelMatrix, camera, this->light);

    // Render the boxes
    for (size_t i = 0; i < collectibles.size(); i++)
        collectibles[i]->render(camera, light);
}

void Track::generatePlayersIDs(int quantity)
{
    for (int i = 0; i < quantity; i++)
    {
        competitorStatus.push_back({-1, 0});
    }
}

/**
 * Test if the competitor reached one of the checkpoints before or after the checkpoint he's in now.
 *
 * The test is made for a number of checkpoints before the current checkpoint, and the same number for after. The number is defined by the CHECKPOINTS_DEPTH constant.
 * The competitor position and lap will also be updated.
 * @param competitorID The ID used to identify the competitor. It can be any number between the number of competitors, but for the same competitor the same number must be used always in all Track methods that requires competitors IDs.
 * @param competitorBoundingBox The bounding box of the competitor to be tested.
 */
void Track::verifyCheckpoints(unsigned int competitorID, BoundingBox2D* competitorBoundingBox)
{
    int competitorCheckpoint = competitorStatus[competitorID].checkpoint;
    // Test a number of checkpoints before and after the checkpoint the competitor is currently in
    for (int i = competitorCheckpoint - CHECKPOINTS_DEPTH; i < competitorCheckpoint + CHECKPOINTS_DEPTH; i++)
    {
        if (i < 0)
            continue; // This means the checkpoint is before lap 0
        if (i == competitorCheckpoint)
            continue; // The checkpoint the competitor is currently in

        int checkNum = i % checkpoints.size();
        bool isInCheckpoint;
        if (checkNum == 0)
            isInCheckpoint = competitorBoundingBox->overlaps(checkpoints[checkNum]);
        else
            isInCheckpoint = competitorBoundingBox->isInside(checkpoints[checkNum]);

        if (isInCheckpoint)
        {
            // Update the checkpoint data
            competitorStatus[competitorID].checkpoint = i;
            competitorStatus[competitorID].checkpointTime = glfwGetTime();
            return;
        }
    }
}

bool Track::verifyCollectibles(BoundingBox2D* competitorBoundingBox)
{
    for (size_t i = 0; i < collectibles.size(); i++)
    {
        if (!collectibles[i]->collected() && competitorBoundingBox->overlaps(collectibles[i]->boundingBox))
        {
            collectibles[i]->collect();
            return true;
        }
    }
    return false;
}

int Track::getLap(unsigned int competitorID)
{
    if (competitorStatus[competitorID].checkpoint < 0)
        return 0;

    return ((int)competitorStatus[competitorID].checkpoint/checkpoints.size()) + 1;
}

int Track::getPosition(unsigned int competitorID)
{
    int position = 1;
    for (size_t i = 0; i < competitorStatus.size(); i++)
    {
        if (i == competitorID)
            continue; // Only compare with other competitors, not self

        // If the other competitor is in a checkpoint ahead, increment the current competitor's position
        if (competitorStatus[i].checkpoint > competitorStatus[competitorID].checkpoint)
            position++;
        // If the other competitor is in the same checkpoint, but he reached it first, increment the current competitor's position
        else if (competitorStatus[i].checkpoint == competitorStatus[competitorID].checkpoint &&
                 competitorStatus[i].checkpointTime < competitorStatus[competitorID].checkpointTime)
            position++;
    }
    return position;
}

void Track::loadTrack(const char* path, GLuint shaderProgram)
{
    std::string modelPath, minimapPath;

    std::string line;
    std::ifstream f(path);

    if(!f.is_open())
    {
        fprintf(stderr, "Error while opening file: %s\n", path);
        return;
    }

    while (getline(f, line))
    {
        if (line[0] == '\0')
            continue; // empty line

        if (line[0] == '#')
            continue; // comment

        // Get the model
        if (line[0] == 'm' && line[1] == ' ')
        {
            std::string path = line.substr(2);
            if (path.find(" ") != -1 && path.substr(0, path.find(" ")) == "$MODELS_PATH")
                modelPath = MODELS_PATH + path.substr(path.find(" ")+1);
            else
                modelPath = path;
            continue;
        }

        // Get the minimap
        if (line[0] == 'm' && line[1] == 'i' && line[2] == ' ')
        {
            std::string path = line.substr(3);
            if (path.find(" ") != -1 && path.substr(0, path.find(" ")) == "$MODELS_PATH")
                minimapPath = MODELS_PATH + path.substr(path.find(" ")+1);
            else
                minimapPath = path;
            continue;
        }

        // Get the map scale
        if (line[0] == 's' && line[1] == ' ')
        {
            this->scale = atof(line.substr(2).c_str());
            continue;
        }

        // Get the number of laps
        if (line[0] == 'l' && line[1] == 'a' && line[2] == ' ')
        {
            this->numLaps = atoi(line.substr(3).c_str());
            continue;
        }

        // Get competitors
        if (line[0] == 'c' && line[1] == ' ')
        {
            int nextPos;
            this->playersStartingPositions.push_back(getVectorFromLine(line.substr(2), &nextPos)*this->scale);
            this->playersStartingDirections.push_back(getVectorFromLine(line.substr(nextPos+2)));
            continue;
        }

        // Get collectibles boxes
        if (line[0] == 'b' && line[1] == ' ')
        {
            this->collectibles.push_back(new Box(getVectorFromLine(line.substr(2))*this->scale, shaderProgram));
            continue;
        }

        // Get checkpoints
        if (line[0] == 'c' && line[1] == 'h' && line[2] == ' ')
        {
            int nextPos;
            glm::vec3 boundCenter = getVectorFromLine(line.substr(3), &nextPos)*this->scale;
            int scalePos = nextPos + 3;
            glm::vec3 boundScale = getVectorFromLine(line.substr(scalePos), &nextPos)*this->scale;
            int directionPos = nextPos + scalePos;
            glm::vec3 boundDirection = getVectorFromLine(line.substr(directionPos));
            this->checkpoints.push_back(new BoundingBox2D(boundCenter, boundScale.x, boundScale.z, boundDirection));
            continue;
        }

        // Get walls
        if (line[0] == 'w' && line[1] == ' ')
        {
            int nextPos;
            glm::vec3 boundCenter = getVectorFromLine(line.substr(2), &nextPos)*this->scale;
            int scalePos = nextPos + 2;
            glm::vec3 boundScale = getVectorFromLine(line.substr(scalePos), &nextPos)*this->scale;
            int directionPos = nextPos + scalePos;
            glm::vec3 boundDirection = getVectorFromLine(line.substr(directionPos));
            this->walls.push_back(new BoundingBox2D(boundCenter, boundScale.x, boundScale.z, boundDirection));
            continue;
        }

        // Get light
        if (line[0] == 'l' && line[1] == ' ')
        {
            int nextPos;
            glm::vec3 lightPosition = getVectorFromLine(line.substr(2), &nextPos)*this->scale;
            int ambientPos = nextPos + 2;
            glm::vec3 lightAmbient = getVectorFromLine(line.substr(ambientPos), &nextPos);
            int diffusePos = nextPos + ambientPos;
            glm::vec3 lightDiffuse = getVectorFromLine(line.substr(diffusePos), &nextPos);
            int specularPos = nextPos + diffusePos;
            glm::vec3 lightSpecular = getVectorFromLine(line.substr(specularPos));
            this->light = new Light(lightPosition, lightAmbient, lightDiffuse, lightSpecular);
            continue;
        }
    }

    f.close();

    model = new Model(modelPath.c_str(), shaderProgram);
    Sprite::sprites["minimap"] = new Sprite(minimapPath.c_str(), true);
}

glm::vec3 getVectorFromLine(std::string line, int* endPos)
{
    int nextPos, initPos, totalPos;
    glm::vec3 vec(0.0, 0.0, 0.0);

    // Get x
    initPos = line.find_first_not_of(" ");
    nextPos = line.substr(initPos).find(" ");
    if (nextPos == -1)
    {
        fprintf(stderr, "Error getting vector from line: %s\n", line.c_str());
        return vec;
    }
    vec.x = atof(line.substr(initPos, nextPos).c_str());
    totalPos = initPos + nextPos;

    // Get y
    initPos = line.substr(totalPos).find_first_not_of(" ") + totalPos;
    nextPos = line.substr(initPos).find(" ");
    if (nextPos == -1)
    {
        fprintf(stderr, "Error getting vector from line: %s\n", line.c_str());
        return vec;
    }
    vec.y = atof(line.substr(initPos, nextPos).c_str());
    totalPos = initPos + nextPos;

    // Get z
    initPos = line.substr(totalPos).find_first_not_of(" ") + totalPos;
    nextPos = line.substr(initPos).find(" ");
    if (nextPos == -1)
    {
        fprintf(stderr, "Error getting vector from line: %s\n", line.c_str());
        return vec;
    }
    vec.z = atof(line.substr(initPos, nextPos).c_str());
    totalPos = initPos + nextPos;

    if (endPos != 0)
        *endPos = totalPos;

    return vec;
}
