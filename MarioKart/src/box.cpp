#include "box.h"

#include "constants.h"

GLfloat Box::rotation = 0.0;

Box::Box(glm::vec3 position, GLuint shaderProgram) :
    position(position)
{
    spawnCounter = 0;
    boundingBox = new BoundingBox2D(position, BOX_SIZE*BOX_SCALE, BOX_SIZE*BOX_SCALE, glm::vec3(0.0, 0.0, 1.0));
    model = new Model(MODELS_PATH "box/3dmodel.obj", shaderProgram);
}

void Box::update(GLfloat deltaTime)
{
    if (spawnCounter > 0)
        spawnCounter -= deltaTime;
}

void Box::render(Camera* camera, Light* light)
{
    if (!collected())
    {
        glm::mat4 modelMatrix;
        // Set the model position
        modelMatrix = glm::translate(modelMatrix, this->position);
        // Set the model rotation
        modelMatrix = glm::rotate(modelMatrix, Box::rotation, glm::vec3(0.0, 0.0, 1.0)); // Rotate in z axis
        modelMatrix = glm::rotate(modelMatrix, Box::rotation, glm::vec3(0.0, 1.0, 0.0)); // Rotate in y axis
        // Set the model scale
        modelMatrix = glm::scale(modelMatrix, glm::vec3(BOX_SCALE, BOX_SCALE, BOX_SCALE));
        // Finally, draw the model
        this->model->draw(modelMatrix, camera, light);
    }
}

void Box::collect()
{
    spawnCounter = BOX_SPAWN_TIME;
}

bool Box::collected()
{
    return spawnCounter > 0;
}
