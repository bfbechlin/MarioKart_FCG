#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include "constants.h"
#include "game.h"
#include "global.h"
#include "menu.h"
#include "scene.h"
#include "sprite.h"

#include <stdio.h>

int main()
{
    int windowWidth, windowHeight;
    GLFWwindow* window;

    // Initialize the library
    printf("Initializing GLFW.\n");
    if (!glfwInit())
        return -1;

    glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);

    // Create a windowed mode window and its OpenGL context
    windowWidth = 1080;
    windowHeight = 720;
    printf("Creating window %dX%d.\n", windowWidth, windowHeight);
    window = glfwCreateWindow(windowWidth, windowHeight, "Mario Kart", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    // Set the window global var and window context
    Global::setWindow(window);

    // Initialize GLEW
    printf("Initializing GLEW.\n");
    glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

    fprintf(stderr,"Using OpenGL VERSION = %s\n", (const char*)glGetString(GL_VERSION));

    // Initialize sprites
    Sprite::spriteInit(RESOURCES_PATH "shaders/sprite.vs", RESOURCES_PATH "shaders/sprite.fs");
    // Create the menu and set it as the current scene
    Scene* currentScene = new Menu();

    // Loop until the user closes the window
    while (!glfwWindowShouldClose(window))
    {
        // Process the inputs, calculate the variations and render the scene
        currentScene->processInput();
        currentScene->step();
        currentScene->render();

        // Check if the scene should switch and, if so, switch to the new one
        if (currentScene->switchScene())
        {
            printf("Switching scenes.\n");
            Scene* nextScene = currentScene->nextScene();
            delete currentScene;
            currentScene = nextScene;
        }

        // Swap front and back buffers
        glfwSwapBuffers(window);

        // Poll for and process events
        glfwPollEvents();

        Global::updateDeltaTime();
    }

    delete currentScene;

    glfwTerminate();
    return 0;
}
