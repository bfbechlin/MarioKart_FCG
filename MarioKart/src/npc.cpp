#include "npc.h"

#include <stdio.h>

NPC::NPC(Model* model, AI* ai) :
    Competitor(model), ai(ai)
{
    Competitor::currentCine.position  = glm::vec3(0.0, 0.0, 0.0);
    Competitor::currentCine.direction = glm::vec3(0.0, 0.0, -1.0);
}

void NPC::processAI(Track* trackInfo)
{
    ai->process(trackInfo, this);
}

void NPC::processAction(std::vector<PowerUp*>* powerUps, bool canMove)
{
    Competitor::boundingBox->movement(Competitor::updatedCine.position, Competitor::updatedCine.direction);
    if (canMove)
    {
        Competitor::calculateMovement(ai->accelerateAction,
                                      ai->brakeAction,
                                      ai->leftAction,
                                      ai->rightAction
        );
        Competitor::processAction(powerUps, ai->useItemAction);
    }
}

void NPC::finishRace(int position)
{

}
