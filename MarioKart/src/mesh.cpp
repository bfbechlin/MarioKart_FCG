#include "mesh.h"

#include <sstream>

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures)
{
    this->vertices = vertices;
    this->indices  = indices;
    this->textures = textures;

    this->setupMesh();
}

void Mesh::draw(GLuint shaderProgram, Camera* camera, Light* light)
{
    GLuint numDiffuse  = 1;
    GLuint numSpecular = 1;
    for (GLuint i = 0; i < textures.size(); i++)
    {
        // Activate the texture unit
        glActiveTexture(GL_TEXTURE0 + i);

        // Retrieve texture number
        std::stringstream stringStream;
        std::string number;
        std::string name = textures[i].type;
        if (name == "texture_diffuse")
            stringStream << numDiffuse++;
        else if (name == "texture_specular")
            stringStream << numSpecular++;
        number = stringStream.str();

        // Set the sampler to the correct texture unit
        glUniform1i(glGetUniformLocation(shaderProgram, (name + number).c_str()), i);
        // Bind the texture
        glBindTexture(GL_TEXTURE_2D, textures[i].id);
    }
    // Set the light and camera position
    glUniform3f(glGetUniformLocation(shaderProgram, "position"), light->position.x, light->position.y, light->position.z);
    glUniform3f(glGetUniformLocation(shaderProgram, "viewPosition"), camera->position.x, camera->position.y, camera->position.z);
    // Set light properties
    glUniform3f(glGetUniformLocation(shaderProgram, "ambient"), light->ambient.r, light->ambient.g, light->ambient.b);
    glUniform3f(glGetUniformLocation(shaderProgram, "diffuse"), light->diffuse.r, light->diffuse.g, light->diffuse.b);
    glUniform3f(glGetUniformLocation(shaderProgram, "specular"), light->specular.r, light->specular.g, light->specular.b);
    // Material shininess
    glUniform1f(glGetUniformLocation(shaderProgram, "shininess"), 32.0f);

    glActiveTexture(GL_TEXTURE0);

    // Draw mesh
    glBindVertexArray(vertexArray);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // Unbind textures
    for (GLuint i = 0; i < textures.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void Mesh::setupMesh()
{
    glGenVertexArrays(1, &vertexArray);
    glGenBuffers(1, &vertexBuffer);
    glGenBuffers(1, &elementBuffer);

    glBindVertexArray(vertexArray);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

    // Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

    // Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

    // Texture coordinators
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, textureCoords));

    // Unbind the vertex array
    glBindVertexArray(0);
}
