#include "keyboard.h"

#include <stdio.h>

#include "constants.h"
#include "global.h"

KEY_T Keyboard::pauseKey = { PAUSE_KEY, false, false };

void checkKeyStatus(KEY_T* keyStruct);

Keyboard::Keyboard(int accelerate, int brake, int left, int right, int useItem, int changeCamera)
{
    // Define the keys for each action
    this->accelerateKey.code = accelerate;
    this->brakeKey.code = brake;
    this->leftKey.code = left;
    this->rightKey.code = right;
    this->useItemKey.code = useItem;
    this->changeCameraKey.code = changeCamera;

    // Set the keys state to unpressed
    this->resetStatus();
}

void Keyboard::processInput()
{
    // Reset the key status before checking
    resetStatus();

    // Check if the pause key was pressed
    checkKeyStatus(&pauseKey);

    // Check if accelerate or brake keys are pressed
    checkKeyStatus(&accelerateKey);
    checkKeyStatus(&brakeKey);

    // Check if right or left keys are pressed
    checkKeyStatus(&leftKey);
    checkKeyStatus(&rightKey);

    // Check if use item or change camera keys are pressed
    checkKeyStatus(&useItemKey);
    checkKeyStatus(&changeCameraKey);
}

void checkKeyStatus(KEY_T* keyStruct)
{
    int keyValue = glfwGetKey(Global::window(), keyStruct->code);
    // Check if pressed
    if (keyValue == GLFW_PRESS)
        keyStruct->pressed = true;
    // Check if released
    else if (keyValue == GLFW_RELEASE)
        keyStruct->pressed = false;
}

void Keyboard::resetStatus()
{
    if (!accelerateKey.pressed)
        accelerateKey.executed = false;
    if (!brakeKey.pressed)
        brakeKey.executed = false;
    if (!rightKey.pressed)
        rightKey.executed = false;
    if (!leftKey.pressed)
        leftKey.executed = false;
    if (!changeCameraKey.pressed)
        changeCameraKey.executed = false;
    if (!useItemKey.pressed)
        useItemKey.executed = false;

    accelerateKey.pressed = false;
    brakeKey.pressed = false;
    rightKey.pressed = false;
    leftKey.pressed = false;
    useItemKey.pressed = false;
    changeCameraKey.pressed = false;

    pauseKey.pressed = false;
}
