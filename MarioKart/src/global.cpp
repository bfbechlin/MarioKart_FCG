#include "global.h"

// Window
GLFWwindow* Global::windowVar;
// Delta time
GLfloat Global::deltaTimeVar = 0.0;
GLfloat Global::lastFrame    = 0.0;
GLfloat Global::currentFrame = 0.0;



// Window

GLFWwindow* Global::window()
{
    return windowVar;
}

void Global::setWindow(GLFWwindow* window)
{
    // Make the window's context current
    glfwMakeContextCurrent(window);
    windowVar = window;
}

void Global::windowSize(int* width, int* height)
{
    glfwGetWindowSize(windowVar, width, height);
}



// Delta time

GLfloat Global::deltaTime()
{
    return deltaTimeVar;
}

void Global::updateDeltaTime()
{
    currentFrame = glfwGetTime();
    deltaTimeVar = currentFrame - lastFrame;
    lastFrame = currentFrame;
}
