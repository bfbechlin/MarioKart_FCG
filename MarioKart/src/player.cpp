#include "player.h"

#include <stdio.h>

#include "constants.h"
#include "global.h"

Player::Player(Model* model, Keyboard* controls) :
    Competitor(model), controls(controls)
{
    endingAI = new AI();
    // Cinematics
    Competitor::currentCine.position  = glm::vec3(0.0, 0.0,  0.0);
    Competitor::currentCine.direction = glm::vec3(0.0, 0.0, -1.0);

    updatedCine = currentCine;
}

void Player::initCamera(float x, float y, float width, float height)
{
    this->camera = new Camera(x, y, width, height);

    // Create the HUD elements
    positionHud = new Hud(TOP_LEFT, 0.0, 0.0, glm::vec2(100.0/width, 100.0/height), 0.0, glm::vec3(-1.0, -1.0, -1.0));
    for(int i = 0; i < MAX_COMPETITORS; i++)
    {
        char buffer[4];
        sprintf(buffer, "%i", i+1);
        std::string filename = "";
        filename = filename + "position" + buffer + ".png";
        positionHud->addNewSprite(Sprite::sprites[filename]);
    }
    powerUpSpaceHud = new Hud(BOTTOM_LEFT, 0.0, 1.0, glm::vec2(100.0/width, 100.0/height), 0.0, glm::vec3(-1.0, -1.0, -1.0));;
    powerUpSpaceHud->addNewSprite(Sprite::sprites["border.png"]);
    powerUpSpaceHud->setCurrentSprite(0);
    powerUpHud = new Hud(BOTTOM_LEFT, 0.0, 1.0, glm::vec2(100.0/width, 100.0/height), 0.0, glm::vec3(-1.0, -1.0, -1.0));
    for(int i = 0; i < NUM_POWERUPS; i++)
    {
        char buffer[4];
        sprintf(buffer, "%i", i+1);
        std::string filename = "";
        filename = filename + "powerUp" + buffer + ".png";
        powerUpHud->addNewSprite(Sprite::sprites[filename]);
    }
    lapHud = new Hud(BOTTOM_LEFT, 0.1, 1.0, glm::vec2(100.0/width, 100.0/height), 0.0, glm::vec3(-1.0, -1.0, -1.0));
    lapHud->addNewSprite(Sprite::sprites["laps.png"]);
    lapHud->setCurrentSprite(0);
}

Camera* Player::getCamera()
{
    return this->camera;
}

void Player::processInput(Track* trackInfo)
{
    // Check if any key for this player is pressed
    controls->processInput();
    // Process AI
    endingAI->process(trackInfo, this);
}

void Player::processAction(std::vector<PowerUp*>* powerUps, bool canMove)
{
    if (!Competitor::finished())
    {
        // Change camera mode
        if (controls->changeCameraKey.pressed && !controls->changeCameraKey.executed)
        {
            camera->circleViewModes();
            controls->changeCameraKey.executed = true;
        }

        bool useItem = false;
        if (controls->useItemKey.pressed && !controls->useItemKey.executed)
        {
            useItem = true;
            controls->useItemKey.executed = true;
        }

        if (canMove)
        {
            Competitor::calculateMovement(controls->accelerateKey.pressed,
                                          controls->brakeKey.pressed,
                                          controls->leftKey.pressed,
                                          controls->rightKey.pressed
            );
            Competitor::processAction(powerUps, useItem);
        }
    }
    else
    {
        if (canMove)
        {
            Competitor::calculateMovement(endingAI->accelerateAction,
                                          endingAI->brakeAction,
                                          endingAI->leftAction,
                                          endingAI->rightAction
            );
            Competitor::processAction(powerUps, endingAI->useItemAction);
        }
    }
}

void Player::render(Light* light)
{
    camera->setViewport(true);
    camera->updatePosition(&(Competitor::currentCine.position), &(Competitor::currentCine.direction));

    // Render self
    Competitor::render(this->camera, light);
}

void Player::renderHud(Text* text, int position, int lap, int numLaps)
{
    camera->setViewport(false);

    positionHud->setCurrentSprite(position-1);
    positionHud->drawHud(camera);
    lapHud->drawHud(camera);

    powerUpSpaceHud->drawHud(camera);
    if (!Competitor::haveEmptySlot())
    {
        powerUpHud->setCurrentSprite(Competitor::getPowerUpID()-1);
        powerUpHud->drawHud(camera);
    }

    int width, height;
    Global::windowSize(&width, &height);
    char buffer[4];
    sprintf(buffer, "%d/%d", lap, numLaps);
    text->renderText(buffer, width*0.18, height-50, 1.0, glm::vec3(0.0, 0.0, 0.0));
}

void Player::finishRace(int position)
{
    camera->setToSpectator();
    Competitor::finishRace(position);
}
