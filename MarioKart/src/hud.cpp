#include "hud.h"

#include "global.h"

Hud::Hud(hudAnchor anchor, float x, float y, glm::vec2 size, GLfloat rotate, glm::vec3 color) :
    anchor(anchor), x(x), y(y), size(size), rotate(rotate), color(color) {}

void Hud::addNewSprite(Sprite* newSprite)
{
    sprites.push_back(newSprite);
}

void Hud::drawHud(Camera* camera)
{
    drawHud(camera, 0.0, 0.0, 1.0, this->rotate, this->color);
}

void Hud::drawHud(Camera* camera, float offsetX, float offsetY, float scale, GLfloat rotate, glm::vec3 color)
{
    int width, height;
    Global::windowSize(&width, &height);
    width *= x + offsetX;
    height *= y + offsetY;
    glm::vec2 position;
    switch (anchor)
    {
    case TOP_LEFT:     position = glm::vec2(width, height);                               break;
    case TOP_RIGHT:    position = glm::vec2(width-(size.x*scale), height);                break;
    case BOTTOM_LEFT:  position = glm::vec2(width, height-(size.y*scale));                break;
    case BOTTOM_RIGHT: position = glm::vec2(width-(size.x*scale), height-(size.y*scale)); break;
    }
    currentSprite->drawSprite(position, this->size*scale, rotate, color, camera->projection);
}

void Hud::setCurrentSprite(int index){
    currentSprite = sprites[index];
}

void Hud::setColor(GLfloat r, GLfloat g, GLfloat b)
{
    color = glm::vec3(r, g, b);
}
