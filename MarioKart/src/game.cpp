#include "game.h"

#include <stdio.h>
#include <sstream>

#include "constants.h"
#include "global.h"
#include "keyboard.h"
#include "sprite.h"
#include "text.h"
#include "track.h"

Text *text;

Game::Game(Track* level, Competitor** competitors, int numCompetitors) :
    track(level)
{
    shouldSwitch = false;
    delayToBegin = 3.0;
    finishPosition = 0;
    glClearColor(0.6, 0.8, 1.0, 1.0); // Blue like the sky

    int numPlayers = 0;

    // Insert the competitors to the players and NPCs vectors
    for (int i = 0; i < numCompetitors; i++)
    {
        // Insert competitor to the competitors vector
        this->competitors.push_back(competitors[i]);
        // Then insert into the corresponding vector depending if it's a player or an NPC
        // Dynamic cast to check if competitor is a players
        Player* p = dynamic_cast<Player*> (competitors[i]);

        // If it's a player, add to the players list. Else it's an NPC, add to the NPCs list
        if (p != NULL)
        {
            this->players.push_back((Player*)competitors[i]);
            numPlayers++;
        }
        else
            this->npcs.push_back((NPC*)competitors[i]);

        // Set initial position, direction of competitors and initialing the current/update cinematics
        this->competitors[i]->currentCine.position  = track->playersStartingPositions[i];
        this->competitors[i]->currentCine.direction = track->playersStartingDirections[i];
        this->competitors[i]->updatedCine = this->competitors[i]->currentCine;
        this->competitors[i]->boundingBox->movement(this->competitors[i]->currentCine.position, this->competitors[i]->currentCine.direction);

        // Set competitor ID
        this->competitors[i]->ID = i;
    }
    track->generatePlayersIDs(numCompetitors);

    // Initialize camera (ONLY WORKS UP FOR FOUR PLAYER FOR NOW)
    switch (numPlayers)
    {
    case 1:
        players[0]->initCamera(0.0, 0.0, 1.0, 1.0);
        break;
    case 2:
        players[0]->initCamera(0.0, 0.5, 1.0, 0.5);
        players[1]->initCamera(0.0, 0.0, 1.0, 0.5);
        break;
    case 3:
        players[0]->initCamera(0.0, 0.5, 1.0, 0.5);
        players[1]->initCamera(0.0, 0.0, 0.5, 0.5);
        players[2]->initCamera(0.5, 0.0, 0.5, 0.5);
        break;
    case 4:
        players[0]->initCamera(0.0, 0.5, 0.5, 0.5);
        players[1]->initCamera(0.5, 0.5, 0.5, 0.5);
        players[2]->initCamera(0.0, 0.0, 0.5, 0.5);
        players[3]->initCamera(0.5, 0.0, 0.5, 0.5);
        break;
    }
    globalCamera = new Camera(0.0, 0.0, 1.0, 1.0);
    int windowWidth, windowHeight;
    Global::windowSize(&windowWidth, &windowHeight);

    text = new Text(windowWidth, windowHeight, RESOURCES_PATH "shaders/text.vs", RESOURCES_PATH "shaders/text.fs");
    text->load(RESOURCES_PATH "fonts/MARIO-KART-FONT.TTF", 48);

    // Create the minimap
    minimap = new Hud(TOP_RIGHT, 0.95, 0.05, glm::vec2(100.0, 100.0), 0, glm::vec3(-1.0, -1.0, -1.0));
    minimap->addNewSprite(Sprite::sprites["minimap"]);
    for (int i = 0; i < MAX_COMPETITORS; i++)
    {
        char buffer[4];
        sprintf(buffer, "%i", i+1);
        std::string filename = "";
        filename = filename + "char" + buffer + ".png";

        minimap->addNewSprite(Sprite::sprites[filename]);
    }
}

void Game::processInput()
{
    // Process the input of each player
    for (size_t i = 0; i < players.size(); i++)
        players[i]->processInput(track);

    for (size_t i = 0; i < npcs.size(); i++)
        if (delayToBegin <= 0)
            npcs[i]->processAI(track);
}

void Game::step()
{
    for (size_t i = 0; i < competitors.size(); i++)
    {
        // Process the actions of the competitors
        competitors[i]->processAction(&powerUps, delayToBegin <= 0);
        // Update position in checkpoints
        track->verifyCheckpoints(i, competitors[i]->boundingBox);
        // Check if finished race
        if (track->getLap(i) > track->numLaps)
            competitors[i]->finishRace(++finishPosition); // Set the player to finish state and increment the finish position counter
        // Check if collected a power up
        if (competitors[i]->haveEmptySlot() && track->verifyCollectibles(competitors[i]->boundingBox))
            competitors[i]->collectPowerUp();
    }

    // Process power ups currently in game
    for (size_t i = 0; i < powerUps.size(); i++)
    {
        powerUps[i]->process(track);
        if (powerUps[i]->finished)
        {
            delete powerUps[i];
            powerUps.erase(powerUps.begin()+i);
            i--;
        }
    }

    // Update track components
    track->update();

    // Verify collisions
    for (size_t i = 0; i < competitors.size(); i++)
    {
        // Check collision between competitors
        // For each competitor, test with the competitors with higher ID. If they collide, set the collision for both.
        if(!competitors[i]->collided){
            for (size_t j = i + 1; j < competitors.size(); j++)
            {
                if (competitors[i]->boundingBox->overlaps(competitors[j]->boundingBox))
                {
                    competitors[i]->collide(competitors[j]);
                }
            }
        }

        // Check collision with walls

        for (size_t j = 0; j < track->walls.size(); j++)
        {
            if (competitors[i]->boundingBox->overlaps(track->walls[j]))
                competitors[i]->collide(track->walls[j]);
        }
        // Check collision with power ups
        for (size_t j = 0; j < powerUps.size(); j++)
        {
            if (powerUps[j]->collides(competitors[i]->boundingBox))
//                competitors[i]->collide(powerUps[j]);
                competitors[i]->collide(powerUps[j]->boundingBox);
        }
        // Update the competitor cinematic
        competitors[i]->currentCine = competitors[i]->updatedCine;
    }

    for (size_t j = 0; j < competitors.size(); j++)
        competitors[j]->collided = false;

    if (delayToBegin > 0){
        delayToBegin -= Global::deltaTime();
        if(delayToBegin <= 0)
            timeInicialGame = glfwGetTime();
    }

    // Check if all players finished the race
    bool gameFinished = true;
    for (size_t i = 0; i < players.size(); i++)
    {
        if (!players[i]->Competitor::finished())
            gameFinished = false;
    }

    if (gameFinished)
        changeToResultsScreen();
}

void Game::render()
{
    // Clear the screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Prepare to render 3D models
    glEnable(GL_DEPTH_TEST);

    // HARDCODED CLOSE, SHOULD OPEN A PAUSE MENU
    if (Keyboard::pauseKey.pressed)
        glfwSetWindowShouldClose(Global::window(), 1);

    // Render each player's screen
    for (size_t i = 0; i < players.size(); i++)
    {
        // Render the player model
        players[i]->render(track->light);
        // Render the track and track components
        this->track->render(players[i]->getCamera());
        // Render the power ups models
        for (size_t j = 0; j < powerUps.size(); j++)
            powerUps[j]->render(players[i]->getCamera(), track->light);
        // Render each of the other competitors
        for (size_t j = 0; j < competitors.size(); j++)
        {
            if (competitors[j] != players[i])
                competitors[j]->render(players[i]->getCamera(), track->light);
        }
    }


    // Prepare to render 2D HUD
    glDisable(GL_DEPTH_TEST);

    // Render each player's screen
    for (size_t i = 0; i < players.size(); i++)
    {
        unsigned int playerID = players[i]->Competitor::ID;
        players[i]->renderHud(text, track->getPosition(playerID), track->getLap(playerID), track->numLaps);
    }

    GLint minutes = 0, seconds = 0, milisec = 0;
    if(timeInicialGame != 0){
        GLfloat deltaTime = glfwGetTime() - timeInicialGame;
        minutes = deltaTime/60;
        seconds = static_cast<int>(deltaTime)%60;
        milisec = static_cast<int>(deltaTime*10)%10;
    }


    // Render the global HUD
    globalCamera->setViewport(false);

    int width, height;
    Global::windowSize(&width, &height);

    // Render timer
    char buffer[10];
    sprintf(buffer, "%02d:%02d.%01d", minutes, seconds, milisec);
    text->renderText("TIME", (width/2)-60, 5.0f, 1.0f, glm::vec3(.949f, 0.258f, 0.211f));
    text->renderText(buffer, (width/2)-80, 40.0f, 1.0f, glm::vec3(.949f, 0.258f, 0.211f));

    // Render the minimap
    minimap->setCurrentSprite(0);
    minimap->drawHud(globalCamera);
    for (size_t i = 0; i < competitors.size(); i++)
    {
        minimap->setCurrentSprite(i+1);
        float offsetX = (competitors[i]->currentCine.position.x/(track->scale*600))-0.04;
        float offsetY = (competitors[i]->currentCine.position.z/(track->scale*400))+0.055;
        minimap->drawHud(globalCamera, offsetX, offsetY, 0.3, 0.0, glm::vec3(-1.0, -1.0, -1.0));
    }
}

bool Game::switchScene()
{
    return shouldSwitch;
}

Scene* Game::nextScene()
{
    return menuScene;
}

void Game::changeToResultsScreen()
{
    for (size_t i = 0; i < npcs.size(); i++)
    {
        if (!npcs[i]->Competitor::finished())
            npcs[i]->Competitor::finishRace(track->getPosition(npcs[i]->Competitor::ID));
    }
    menuScene = new Menu(competitors);
    shouldSwitch = true;
}
