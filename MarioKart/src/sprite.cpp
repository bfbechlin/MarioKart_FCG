#include "sprite.h"

#include "constants.h"

std::map<std::string, Sprite*> Sprite::sprites;
GLint Sprite::programShader;

void Sprite::drawSprite(glm::vec2 position, glm::vec2 size, GLfloat rotate, glm::vec3 color, glm::mat4 projection)
{
    // Prepare transformations
    glUseProgram(programShader);

    glm::mat4 model;
    model = glm::translate(model, glm::vec3(position, 0.0f));  // First translate (transformations are: scale happens first, then rotation and then finally translation happens; reversed order)
    model = glm::translate(model, glm::vec3(0.5 * size.x, 0.5 * size.y, 0.0)); // Move origin of rotation to center of quad
    model = glm::rotate(model, rotate, glm::vec3(0.0f, 0.0f, 1.0f)); // Then rotate
    model = glm::translate(model, glm::vec3(-0.5f * size.x, -0.5f * size.y, 0.0f)); // Move origin back
    model = glm::scale(model, glm::vec3(size, 1.0f)); // Last scale

    glUniformMatrix4fv(glGetUniformLocation(programShader, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(glGetUniformLocation(programShader, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

//    glm::vec3 loucura = glm::vec3(model*projection*glm::vec4(0.0, 0.0, 0.0, 1.0));
//    printf("%f %f %f\n", loucura.x, loucura.y, loucura.z);

    // Render textured quad
    glUniform3f(glGetUniformLocation(programShader, "spriteColor"), color.x, color.y, color.z);

    glActiveTexture(GL_TEXTURE0);
    this->texture.Bind();

    glBindVertexArray(this->quadVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

Sprite::Sprite(const char* texturePath, bool alpha){
    this->texture = Texture2D::loadTextureFromFile(texturePath, alpha);
    this->initRenderData();
}

void Sprite::spriteInit(const char *vShaderFile, const char *fShaderFile){
    programShader = loadShaders(vShaderFile, fShaderFile);

    // Border, coin and laps sprites
    Sprite::sprites["border.png"] = new Sprite(SPRITES_PATH "border.png", true);
    Sprite::sprites["coin.png"]   = new Sprite(SPRITES_PATH "coin.png", true);
    Sprite::sprites["laps.png"]   = new Sprite(SPRITES_PATH "laps.png", true);
    // Initialize position sprites
    for (int i = 0; i < MAX_COMPETITORS; i++)
    {
        char buffer[4];
        sprintf(buffer, "%i", i+1);
        std::string filename = "";
        filename = filename + "position" + buffer + ".png";

        Sprite::sprites[filename] = new Sprite((SPRITES_PATH + filename).c_str(), true);
    }
    // Initialize power ups sprites
    for (int i = 0; i < NUM_POWERUPS; i++)
    {
        char buffer[4];
        sprintf(buffer, "%i", i+1);
        std::string filename = "";
        filename = filename + "powerUp" + buffer + ".png";

        Sprite::sprites[filename] = new Sprite((SPRITES_PATH + filename).c_str(), true);
    }
    // Initialize characters sprites
    for (int i = 0; i < MAX_COMPETITORS; i++)
    {
        char buffer[4];
        sprintf(buffer, "%i", i+1);
        std::string filename = "";
        filename = filename + "char" + buffer + ".png";

        Sprite::sprites[filename] = new Sprite((SPRITES_PATH + filename).c_str(), true);
    }
}

void Sprite::initRenderData()
{
    // Configure VAO/VBO
    GLuint VBO;
    GLfloat vertices[] = {
        // Pos      // Tex
        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f,

        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f
    };

    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(quadVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
