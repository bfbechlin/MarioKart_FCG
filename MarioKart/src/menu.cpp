#include "menu.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include <common/shader.hpp>

#include "ai.h"
#include "competitor.h"
#include "constants.h"
#include "global.h"
#include "keyboard.h"
#include "npc.h"
#include "player.h"
#include "powerup.h"
#include "track.h"

#include <stdio.h>
#include <string>

Menu::Menu()
{
    screen = MAIN_SCREEN;
    initVars();

    glEnable(GL_DOUBLEBUFFER);
    glfwSwapInterval(1);
}

Menu::Menu(std::vector<Competitor*> competitors)
{
    screen = RACE_RESULTS;
    initVars();
}

void Menu::processInput()
{
    keyboard->processInput();
}

void Menu::step()
{
    if (Keyboard::pauseKey.pressed)
        glfwSetWindowShouldClose(Global::window(), 1);


    if (keyboard->brakeKey.pressed && !keyboard->brakeKey.executed)
    {
        selection++;
        keyboard->brakeKey.executed = true;
    }

    if (keyboard->accelerateKey.pressed && !keyboard->accelerateKey.executed && selection > 0)
    {
        selection--;
        keyboard->accelerateKey.executed = true;
    }
    switch (screen)
    {
    case MAIN_SCREEN:
        if (selection > 1) selection = 1;
        if (keyboard->useItemKey.pressed && !keyboard->useItemKey.executed)
        {
            switch (selection)
            {
            case 0:
                screen = CHAR_SELECT;
                break;
            case 1:
                glfwSetWindowShouldClose(Global::window(), 1);
                break;
            }
            keyboard->useItemKey.executed = true;
        }
        break;

    case CHAR_SELECT:
        if (selection > MAX_COMPETITORS+1) selection = MAX_COMPETITORS+1;
        if (keyboard->useItemKey.pressed && !keyboard->useItemKey.executed)
        {
            if (selection > 0)
            {
                competitors[selection-1] = new Player(new Model((CHARACTERS_PATH + characters[selection-1] + "/3dmodel.obj").c_str(), defaultShader), plControls[selectedPlayers++]);
                if (selectedPlayers >= numPlayers)
                {
                    for (int i = 0; i < MAX_COMPETITORS; i++)
                    {
                        if (competitors[i] == NULL)
                            competitors[i] = new NPC(new Model((CHARACTERS_PATH + characters[selection-1] + "/3dmodel.obj").c_str(), defaultShader), new AI());
                    }
                    gameScene = new Game(gameTrack, competitors, MAX_COMPETITORS);
                    shouldSwitch = true;
                }
            }
            keyboard->useItemKey.executed = true;
        }
        break;

    case OPTIONS:
        break;

    case RACE_RESULTS:
        screen = MAIN_SCREEN;
        break;
    }
//    // HARDCODED SCENE SWITCH DIRECT TO GAME
//    // Create the default shader
//    GLuint defaultShader = loadShaders(DEFAULT_VERTEX_SHADER, DEFAULT_FRAGMENT_SHADER);
//
//    // Create the track
//    Track* gameTrack = new Track(TRACKS_PATH "simple_track.trdesc", defaultShader);
//    Competitor* competitors[MAX_COMPETITORS];
//
//    // Initialize power ups
//    PowerUp::initialize(defaultShader);
//
//    // Get all the available characters
//    std::string characters[] = {AVAILABLE_CHARACTERS};
//
//    // Create the players controls
//    Keyboard* plControl[4];
//    plControl[0] = new Keyboard(GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, GLFW_KEY_SPACE, GLFW_KEY_V);
//    plControl[1] = new Keyboard(GLFW_KEY_KP_5, GLFW_KEY_KP_2, GLFW_KEY_KP_1, GLFW_KEY_KP_3, GLFW_KEY_KP_0, GLFW_KEY_KP_9);
//    plControl[2] = new Keyboard(GLFW_KEY_UP, GLFW_KEY_DOWN, GLFW_KEY_LEFT, GLFW_KEY_RIGHT, GLFW_KEY_RIGHT_CONTROL, GLFW_KEY_RIGHT_SHIFT);
//    plControl[3] = new Keyboard(GLFW_KEY_I, GLFW_KEY_K, GLFW_KEY_J, GLFW_KEY_L, GLFW_KEY_N, GLFW_KEY_O);
//
//    int i;
//    // Create the player
//    printf("Creating player.\n");
//    for (i = 0; i < NUM_PLAYERS; i++)
//    {
//        competitors[i] = new Player(new Model((CHARACTERS_PATH + characters[i] + "/3dmodel.obj").c_str(), defaultShader), plControl[i]);
//    }
//
//    // Create the NPCs
//    printf("Creating NPCs.\n");
//    for (; i < MAX_COMPETITORS; i++)
//    {
//        competitors[i] = new NPC(new Model((CHARACTERS_PATH + characters[i] + "/3dmodel.obj").c_str(), defaultShader), new AI());
//    }
//
//    // Create the game scene and set the shouldSwitch variable to true
//    printf("Creating game scene.\n");
//    gameScene = new Game(gameTrack, competitors, MAX_COMPETITORS);
//    shouldSwitch = true;
}

void Menu::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);

    camera->setViewport(false);
    switch (screen)
    {
    case MAIN_SCREEN:
        if (selection == 0)
            text->renderText("NEW RACE", 0.0, 5.0, 1.0f, glm::vec3(1.0, 1.0, 1.0));
        else
            text->renderText("NEW RACE", 0.0, 5.0, 1.0f, glm::vec3(1.0, 1.0, 0.4));

        if (selection == 1)
            text->renderText("EXIT", 0.0, 60.0, 1.0f, glm::vec3(1.0, 1.0, 1.0));
        else
            text->renderText("EXIT", 0.0, 60.0, 1.0f, glm::vec3(1.0, 1.0, 0.4));
        break;

    case CHAR_SELECT:
        char buffer[4];
        sprintf(buffer, "%d", numPlayers);
        text->renderText("NUM PLAYERS", 0.0, 5.0, 1.0f, (selection == 0) ? glm::vec3(1.0, 1.0, 1.0) : glm::vec3(1.0, 1.0, 0.4));
        text->renderText(buffer, 0.0, 60.0, 1.0f, (selection == 0) ? glm::vec3(1.0, 1.0, 1.0) : glm::vec3(1.0, 1.0, 0.4));
        break;

    case OPTIONS:
        break;

    case RACE_RESULTS:
        break;
    }
}

bool Menu::switchScene()
{
    // Return if the scene should switch to a new scene
    return shouldSwitch;
}

Scene* Menu::nextScene()
{
    return gameScene;
}

void Menu::initVars()
{
    shouldSwitch = false;

    int windowWidth, windowHeight;
    Global::windowSize(&windowWidth, &windowHeight);
    text = new Text(windowWidth, windowHeight, RESOURCES_PATH "shaders/text.vs", RESOURCES_PATH "shaders/text.fs");
    text->load(RESOURCES_PATH "fonts/MARIO-KART-FONT.TTF", 48);

    camera = new Camera(0.0, 0.0, 1.0, 1.0);
    selectedPlayers = 0;
    numPlayers = 1;
    selection = 0;
    keyboard = new Keyboard(GLFW_KEY_UP, GLFW_KEY_DOWN, GLFW_KEY_LEFT, GLFW_KEY_RIGHT, GLFW_KEY_ENTER, GLFW_KEY_BACKSPACE);

    for (int i = 0; i < MAX_COMPETITORS; i++)
        competitors[i] = NULL;

    std::string chars[8] = {AVAILABLE_CHARACTERS};
    for (int i = 0; i < 8; i++)
        characters[i] = chars[i];

    plControls[0] = new Keyboard(GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, GLFW_KEY_SPACE, GLFW_KEY_V);
    plControls[1] = new Keyboard(GLFW_KEY_KP_5, GLFW_KEY_KP_2, GLFW_KEY_KP_1, GLFW_KEY_KP_3, GLFW_KEY_KP_0, GLFW_KEY_KP_9);
    plControls[2] = new Keyboard(GLFW_KEY_UP, GLFW_KEY_DOWN, GLFW_KEY_LEFT, GLFW_KEY_RIGHT, GLFW_KEY_RIGHT_CONTROL, GLFW_KEY_RIGHT_SHIFT);
    plControls[3] = new Keyboard(GLFW_KEY_I, GLFW_KEY_K, GLFW_KEY_J, GLFW_KEY_L, GLFW_KEY_N, GLFW_KEY_O);

    defaultShader = loadShaders(DEFAULT_VERTEX_SHADER, DEFAULT_FRAGMENT_SHADER);
    gameTrack = new Track(TRACKS_PATH "simple_track.trdesc", defaultShader);
    PowerUp::initialize(defaultShader);
}
