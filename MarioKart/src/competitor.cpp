#include "competitor.h"
#include "constants.h"
#include <stdio.h>
#include <math.h>
#include "global.h"

Competitor::Competitor(Model* model) :
    model(model)
{
    // Initial direction
    currentCine.direction = glm::vec3(0.0, 0.0, -1.0);
    // Initial velocity
    currentCine.velocityModule = 0;
    // Initial rotation angle
    currentCine.angle = 0;

    // Collision status
    collisionCine.direction = glm::vec3(0.0, 0.0, 0.0);
    collisionCine.velocityModule = 0;
    collisionCine.angle = 0;
    collided = false;

    kartStats.braking = 40;
    kartStats.acceleration = 20;

    kartStats.handling = 0.5;
    kartStats.velocityCurvature = 0.5;
    kartStats.baseAngle = 1.5;

    kartStats.decreVelocity = 15;
    kartStats.maxVelocity = 30;
    kartStats.maxVelocityBack = -10;

    leftTime  = 0;
    rightTime = 0;

    boundingBox = new BoundingBox2D(currentCine.position, 0.9, 0.8, currentCine.direction);

    powerUp  = NULL;
    turboTime = 0.0;
    finishedRace = false;
    racePosition = 0;
}

void Competitor::processAction(std::vector<PowerUp*>* powerUps, bool usePowerUp)
{
    if (usePowerUp)
    {
        if (powerUp != NULL)
        {
            powerUpType pType = powerUp->use(currentCine.position, currentCine.direction);
            powerUps->push_back(powerUp);
            powerUp = NULL;

            if (pType == MUSHROOM)
                turboTime = TURBO_TIME;
        }
    }
}

void Competitor::render(Camera* camera, Light* light)
{
    // Render the player model
    glm::mat4 modelMatrix;
    // Set the model position
    modelMatrix = glm::translate(modelMatrix, currentCine.position);
    // Set the model rotation
    GLfloat angle = glm::orientedAngle(glm::vec3(0.0, 0.0, 1.0), currentCine.direction, glm::vec3(0.0, 1.0, 0.0));
    modelMatrix = glm::rotate(modelMatrix, angle, glm::vec3(0.0, 1.0, 0.0));
    // Set the model scale
    modelMatrix = glm::scale(modelMatrix, glm::vec3(1.0, 1.0, 1.0));
    // Finally, draw the model
    this->model->draw(modelMatrix, camera, light);
}

void Competitor::calculateMovement(bool accelerate, bool brake, bool left, bool right)
{
    if (!collided)
        processMovement(accelerate, brake, left, right);
    else
        processCollision();

    this->boundingBox->movement(updatedCine.position, updatedCine.direction);
}

void Competitor::collide(Competitor* other)
{
    GLfloat directionAngle = glm::orientedAngle(updatedCine.direction, other->updatedCine.direction, glm::vec3(0.0, 1.0, 0.0));
    glm::vec3 centerVector(other->currentCine.position.x - currentCine.position.x, 0.0f, other->currentCine.position.z - currentCine.position.z);

    other->collided = true;
    collided = true;

    // FRONTAL COLLISION
    GLfloat angleVectorCenter = glm::orientedAngle(other->updatedCine.direction, glm::normalize(centerVector), glm::vec3(0.0, 1.0, 0.0));
//    printf("ENTRO NA COLISAO!!! angleCenter: %.2f angleDirection: %.2f \n", angleVectorCenter*180/PI, directionAngle*180/PI);
    if(std::abs(angleVectorCenter) < PI/4){
        if(std::abs(directionAngle) > PI/2){
            updatedCine.position = currentCine.position;
            other->updatedCine.position = other->currentCine.position;
//            printf("COLLISION FRONT FRONT %.2f %.2f\n", updatedCine.velocityModule, other->updatedCine.velocityModule);
            GLfloat swap = updatedCine.velocityModule;
            updatedCine.velocityModule = -1*other->updatedCine.velocityModule;
            other->updatedCine.velocityModule = -swap;
        }
        else{
            GLfloat deltaVelocity = std::abs(updatedCine.velocityModule - other->updatedCine.velocityModule);
            updatedCine.position = currentCine.position;
//            printf("COLLISION FRONT BACK %.2f %.2f\n", updatedCine.velocityModule, other->updatedCine.velocityModule);
            updatedCine.velocityModule -= deltaVelocity;
            other->updatedCine.velocityModule += deltaVelocity;

        }
    }
    else if(std::abs(angleVectorCenter) > 3*PI/4){
        if(std::abs(directionAngle) > PI/2){
            updatedCine.position = currentCine.position;
            other->updatedCine.position = other->currentCine.position;
//            printf("COLLISION FRONT FRONT %.2f %.2f\n", updatedCine.velocityModule, other->updatedCine.velocityModule);
            GLfloat swap = updatedCine.velocityModule;
            updatedCine.velocityModule = -1*other->updatedCine.velocityModule;
            other->updatedCine.velocityModule = -swap;
        }
        else{
            GLfloat deltaVelocity = std::abs(updatedCine.velocityModule - other->updatedCine.velocityModule);
            updatedCine.position = currentCine.position;
//            printf("COLLISION FRONT BACK %.2f %.2f\n", updatedCine.velocityModule, other->updatedCine.velocityModule);
            updatedCine.velocityModule += deltaVelocity;
            other->updatedCine.velocityModule -= deltaVelocity;

        }
    }
    else{
        if(std::abs(directionAngle) > PI/2){
            updatedCine.position = currentCine.position;
            other->updatedCine.position = other->currentCine.position;
//            printf("COLLISION FRONT FRONT %.2f %.2f\n", updatedCine.velocityModule, other->updatedCine.velocityModule);
            GLfloat swap = updatedCine.velocityModule;
            updatedCine.velocityModule = -1*other->updatedCine.velocityModule;
            other->updatedCine.velocityModule = -swap;
        }
        else{
            GLfloat deltaVelocity = std::abs(updatedCine.velocityModule - other->updatedCine.velocityModule);
            updatedCine.position = currentCine.position;
//            printf("COLLISION FRONT BACK %.2f %.2f\n", updatedCine.velocityModule, other->updatedCine.velocityModule);
            updatedCine.velocityModule -= deltaVelocity;
            other->updatedCine.velocityModule += deltaVelocity;
        }
    }
}

void Competitor::collide(BoundingBox2D* obstacle)
{
    //collided = true;

    updatedCine = currentCine;
    updatedCine.velocityModule = 0;

}

void Competitor::collide(PowerUp* powerUp)
{
    switch (powerUp->type)
    {
    case BANANA_SKIN:

        break;
    }
}

bool Competitor::haveEmptySlot()
{
    return powerUp == NULL;
}

void Competitor::collectPowerUp()
{
    powerUp = new PowerUp();
}

int Competitor::getPowerUpID()
{
    return powerUp->getID();
}

void Competitor::finishRace(int position)
{
    finishedRace = true;
    racePosition = position;
}

bool Competitor::finished()
{
    return finishedRace;
}

void Competitor::processMovement(bool accelerate, bool brake, bool left, bool right)
{
    if (accelerate && !brake)
    {
        if (updatedCine.velocityModule < kartStats.maxVelocity || (turboTime > 0.0 && updatedCine.velocityModule < TURBO_SPEED))
            updatedCine.velocityModule += kartStats.acceleration*Global::deltaTime();
        else
            updatedCine.velocityModule -= kartStats.acceleration*Global::deltaTime();
    }
    else if (brake && !accelerate)
    {
        if (updatedCine.velocityModule > kartStats.maxVelocityBack)
            updatedCine.velocityModule -= kartStats.braking*Global::deltaTime();
        else
            updatedCine.velocityModule += kartStats.braking*Global::deltaTime();
    }
    else
    {
        if (updatedCine.velocityModule > 0)
        {
            updatedCine.velocityModule -= kartStats.decreVelocity*Global::deltaTime();
            if (updatedCine.velocityModule < 0)
            {
                updatedCine.velocityModule = 0;
            }
        }
        else if (updatedCine.velocityModule < 0)
        {
            updatedCine.velocityModule += kartStats.decreVelocity*Global::deltaTime();
            if(updatedCine.velocityModule > 0)
            {
                updatedCine.velocityModule = 0;
            }
        }
    }

    int curveDirection = 0;
    if (updatedCine.velocityModule > 0)
        curveDirection = 1;
    else if (updatedCine.velocityModule < 0)
        curveDirection = -1;

    if (left && !right)
    {
        rightTime = 0;
        if (leftTime == 0)
            leftTime = glfwGetTime();
        updatedCine.angle =  curveDirection*kartStats.baseAngle*computeCurve(leftTime)*Global::deltaTime();
    }
    else if (right && !left)
    {
        leftTime = 0;
        if (rightTime == 0)
            rightTime = glfwGetTime();
        updatedCine.angle = -curveDirection*kartStats.baseAngle*computeCurve(rightTime)*Global::deltaTime();
    }
    else
    {
        leftTime = 0;
        rightTime = 0;
        if (updatedCine.angle > 0)
        {
            updatedCine.angle -= kartStats.baseAngle*computeCurve(0)*Global::deltaTime()/5;
            if (updatedCine.angle < 0)
                updatedCine.angle = 0;
        }
        else if (updatedCine.angle < 0)
        {
            updatedCine.angle += kartStats.baseAngle*computeCurve(0)*Global::deltaTime()/5;
            if (updatedCine.angle > 0)
                updatedCine.angle = 0;
        }
    }

    glm::vec3 velocityVector = updatedCine.direction*updatedCine.velocityModule;
    updatedCine.position += velocityVector*Global::deltaTime();

    glm::mat4 rotationMat = glm::rotate(updatedCine.angle, glm::vec3(0.0, 1.0, 0.0));
    updatedCine.direction = glm::vec3(rotationMat*glm::vec4(updatedCine.direction, 0.0));

    if (turboTime > 0.0)
        turboTime -= Global::deltaTime();
}

void Competitor::processCollision()
{

}

GLfloat Competitor::computeCurve(GLfloat lastActTime){
    GLfloat deltaTime = glfwGetTime() - lastActTime;
    GLfloat coefTime = (deltaTime - kartStats.handling)/kartStats.handling + 1;
    GLfloat coefVelocity = ((kartStats.maxVelocity-updatedCine.velocityModule)/kartStats.maxVelocity)*kartStats.velocityCurvature + 1;
    if(coefTime <= 1)
    {
        return (coefTime+0.5)*coefVelocity;
    }
    else
    {
        return coefVelocity;
    }
}
