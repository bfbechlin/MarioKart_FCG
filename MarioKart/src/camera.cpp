#include "camera.h"

#include <stdio.h>

#include "global.h"

/**
 * Creates a new camera in the specified virtual position and with the specified virtual resolution.
 *
 * @param x A value between 0.0 and 1.0. Set the position x of the camera relative to a screen with resolution 1x1.
 * @param y A value between 0.0 and 1.0. Set the position y of the camera relative to a screen with resolution 1x1.
 * @param width A value between 0.0 and 1.0. Set the width of the camera relative to a screen with resolution 1x1.
 * @param height A value between 0.0 and 1.0. Set the height of the camera relative to a screen with resolution 1x1.
 */
Camera::Camera(float x, float y, float width, float height) :
    x(x), y(y), width(width), height(height)
{
    // Set the initial camera mode
    currentMode = THIRD_PERSON_2;
    updateModeProperties();
    angle = 3.14;
    // Set the initial zoom and up vector
    upVec = glm::vec3(0.0f, 1.0f,  0.0f);
}

void Camera::setViewport(bool perspective)
{
    int windowWidth, windowHeight;

    // Get the screen width and height
    Global::windowSize(&windowWidth, &windowHeight);
    glViewport(windowWidth*x, windowHeight*y, windowWidth*width, windowHeight*height);

    if (perspective)
        projection = glm::perspective(glm::radians(zoom), ((float)windowWidth*width) / ((float)windowHeight*height), 0.1f, 100.0f);
    else
        projection = glm::ortho(0.0, (double)windowWidth, (double)windowHeight, 0.0);
}

void Camera::updatePosition(glm::vec3* playerPosition, glm::vec3* direction)
{
    // Calculate the rotation relative to the player's direction of movement
    GLfloat expectedAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), glm::normalize(*direction), glm::vec3(0.0f, 1.0f, 0.0f));
    if (expectedAngle - angle > glm::radians(180.0))
        angle += glm::radians(360.0);
    else if (expectedAngle - angle < -glm::radians(180.0))
        angle -= glm::radians(360.0);
    angle += Global::deltaTime()*(expectedAngle - angle)/(rotationDelayFactor*0.1);

    // Calculate the transformation matrix
    glm::mat4 transfMatrix = glm::translate(*playerPosition);
    transfMatrix = glm::rotate(transfMatrix, angle, glm::vec3(0.0, 1.0, 0.0));

    // Apply the transformation matrix to the camera position and the center of view
    glm::vec4 positionVec4(positionOffset, 1.0f);
    glm::vec4 centerVec4(centerOffset, 1.0f);
    positionVec4 = transfMatrix * positionVec4;
    centerVec4 = transfMatrix * centerVec4;

    position = glm::vec3(positionVec4);

    view = glm::lookAt(glm::vec3(positionVec4), glm::vec3(centerVec4), upVec);
}

void Camera::circleViewModes()
{
    // Change the current mode
    switch (currentMode)
    {
        case FIRST_PERSON:   currentMode = THIRD_PERSON_1; break;
        case THIRD_PERSON_1: currentMode = THIRD_PERSON_2; break;
        case THIRD_PERSON_2: currentMode = THIRD_PERSON_3; break;
        case THIRD_PERSON_3: currentMode = FIRST_PERSON;   break;
    }
    this->updateModeProperties();
}

void Camera::setToSpectator()
{
    currentMode = SPECTATOR;
    this->updateModeProperties();
}

void Camera::updateModeProperties()
{
    // Set the properties of the selected mode
    switch (currentMode)
    {
    case FIRST_PERSON:
        positionOffset = glm::vec3(0.0f, 1.0f, 0.8f);
        centerOffset   = glm::vec3(0.0f, 1.0f, 2.0f);
        rotationDelayFactor = 1.0;
        zoom = 45.0f;
        break;

    case THIRD_PERSON_1:
        positionOffset = glm::vec3(0.0f, 2.5f, -3.2f);
        centerOffset   = glm::vec3(0.0f, 0.0f,  4.0f);
        rotationDelayFactor = 1.0;
        zoom = 45.0f;
        break;

    case THIRD_PERSON_2:
        positionOffset = glm::vec3(0.0f, 3.0f, -5.0f);
        centerOffset   = glm::vec3(0.0f, 0.0f,  4.0f);
        rotationDelayFactor = 1.0;
        zoom = 45.0f;
        break;

    case THIRD_PERSON_3:
        positionOffset = glm::vec3(0.0f, 3.0f, -8.0f);
        centerOffset   = glm::vec3(0.0f, 0.0f,  4.0f);
        rotationDelayFactor = 10.0;
        zoom = 80.0f;
        break;


    case SPECTATOR:
        positionOffset = glm::vec3(-1.0f, 0.3f,  3.0f);
        centerOffset   = glm::vec3(1.0f, 0.8f, 0.0f);
        rotationDelayFactor = 1.0;
        zoom = 45.0f;
        break;
    }
}
