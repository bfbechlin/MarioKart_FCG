#include "powerup.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "constants.h"
#include "global.h"

std::map<powerUpType, Model*> PowerUp::models;

void PowerUp::initialize(GLfloat shaderProgram)
{
    models[BANANA_SKIN] = new Model(MODELS_PATH "banana/3dmodel.obj", shaderProgram);
//    models[GREEN_SHELL] = new Model(MODELS_PATH "green_shell/3dmodel.obj", shaderProgram);
}

PowerUp::PowerUp()
{
    srand(time(NULL));
//    type = GREEN_SHELL;
    type = static_cast<powerUpType>(rand() % last);

    used = false;
    finished = false;
}

void PowerUp::render(Camera* camera, Light* light)
{
    if (used)
    {
        switch (type)
        {
        case BANANA_SKIN:
//        case GREEN_SHELL:
            // Render the powerUp model
            glm::mat4 modelMatrix;
            // Set the model position
            modelMatrix = glm::translate(modelMatrix, this->position);
            // Set the model rotation
            GLfloat angle = glm::orientedAngle(glm::vec3(0.0, 0.0, 1.0), this->direction, glm::vec3(0.0, 1.0, 0.0));
            modelMatrix = glm::rotate(modelMatrix, angle, glm::vec3(0.0, 1.0, 0.0));
            // Set the model scale
            modelMatrix = glm::scale(modelMatrix, glm::vec3(this->scale, this->scale, this->scale));
            // Finally, draw the model
            this->models[type]->draw(modelMatrix, camera, light);
            break;
        }
    }
}

powerUpType PowerUp::use(glm::vec3 position, glm::vec3 direction)
{
    if (!used)
    {
        switch (type)
        {
        case BANANA_SKIN:
            this->position = position - glm::normalize(direction)*0.8f;
            this->direction = direction;
            this->scale = BANANA_SCALE;
            this->boundingBox = new BoundingBox2D(this->position, 6.0*BANANA_SCALE, 6.0*BANANA_SCALE, this->direction);
            break;
//        case GREEN_SHELL:
//            this->position = position + glm::normalize(direction)*-8.0f;
//            this->direction = direction;
//            this->scale = SHELL_SCALE;
//            this->boundingBox = new BoundingBox2D(this->position, 6.0*SHELL_SCALE, 6.0*SHELL_SCALE, this->direction);
//            break;
        case MUSHROOM:
            this->finished = true;
            break;
        }
    }
    used = true;
    return this->type;
}

void PowerUp::process(Track* track)
{
    if (used)
    {
        switch (type)
        {
//        case GREEN_SHELL:
//            float speed = 35.0f;
//            this->position += Global::deltaTime()*this->direction*speed;
//            this->boundingBox->movement(position, direction);
////            for (size_t i = 0; i < track->walls.size(); i++)
////            {
////                if (this->boundingBox->isInside(track->walls[i]))
////                {
////                    printf("::::%d\n", i);
////                    track->walls[i]->printData();
////                    boundingBox->printData();
////                    this->finished = true;
////                }
////            }
//            break;
        }
    }
}

bool PowerUp::collides(BoundingBox2D* target)
{
    switch (type)
    {
    case BANANA_SKIN:
//    case GREEN_SHELL:
        if (target->overlaps(this->boundingBox))
        {
            this->finished = true;
            return true;
        }
        break;
    }
    return false;
}

int PowerUp::getID()
{
    switch (type)
    {
    case MUSHROOM:    return 1;
    case BANANA_SKIN: return 3;
//    case GREEN_SHELL: return 5;
    }
}
