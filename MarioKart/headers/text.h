#ifndef TEXT_H
#define TEXT_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <glm/glm.hpp>

#include <map>

#include "texture.h"
#include "common/shader.hpp"

/// Holds all state information relevant to a character as loaded using FreeType
struct Character {
    GLuint TextureID;   // ID handle of the glyph texture
    glm::ivec2 Size;    // Size of glyph
    glm::ivec2 Bearing; // Offset from baseline to left/top of glyph
    GLuint Advance;     // Horizontal offset to advance to next glyph
};


// A renderer class for rendering text displayed by a font loaded using the
// FreeType library. A single font is loaded, processed into a list of Character
// items for later rendering.
class Text
{
public:
    static glm::mat4 projection;
    // Holds a list of pre-compiled Characters
    std::map<GLchar, Character> Characters;
    // Shader used for text rendering
    GLint TextShader;
    // Constructor
    Text(GLuint width, GLuint height, const char *vShaderFile, const char *fShaderFile);
    // Pre-compiles a list of characters from the given font
    void load(std::string font, GLuint fontSize);
    // Renders a string of text using the precompiled list of characters
    void renderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color = glm::vec3(1.0f));
    // Set matrix projection
    void setProjection(GLuint width, GLuint height);
private:
    // Render state
    GLuint VAO, VBO;
};

#endif // TEXT_H
