#ifndef HUD_H
#define HUD_H

#define GLEW_STATIC
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <sstream>
#include <vector>

#include "camera.h"
#include "sprite.h"

enum hudAnchor { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };

class Hud
{
public:
    static glm::mat4 projection;
    std::vector<Sprite*> sprites;

    Hud(hudAnchor anchor, float x, float y, glm::vec2 size, GLfloat rotate, glm::vec3 color);
    void addNewSprite(Sprite* newSprite);

    void drawHud(Camera* camera);
    void drawHud(Camera* camera, float offsetX, float offsetY, float scale, GLfloat rotate, glm::vec3 color);

    void setCurrentSprite(int index);
    void setColor(GLfloat r, GLfloat g, GLfloat b);

private:
    // Location of the sprite center
    hudAnchor anchor;
    float x, y; //0 - 1
    float heiFloating; //0 - 1
    GLfloat rotate;
    glm::vec2 size;
    glm::vec3 color;

    Sprite* currentSprite;
};

#endif // HUD_H
