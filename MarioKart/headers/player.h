#ifndef PLAYER_H
#define PLAYER_H

#include "competitor.h"

#include "ai.h"
#include "camera.h"
#include "hud.h"
#include "keyboard.h"
#include "text.h"

class Player : public Competitor
{
public:
    Player(Model* model, Keyboard* controls);

    void initCamera(float x, float y, float width, float height);
    Camera* getCamera();

    void processInput(Track* trackInfo);
    virtual void processAction(std::vector<PowerUp*>* powerUps, bool canMove);
    void render(Light* light); // Render for self
    void renderHud(Text* text, int position, int lap, int numLaps);

    virtual void finishRace(int position);

private:
    Camera* camera;
    Keyboard* controls;
    AI* endingAI;

    Hud* positionHud;
    Hud* lapHud;
    Hud* powerUpHud;
    Hud* powerUpSpaceHud;
};

#endif // PLAYER_H
