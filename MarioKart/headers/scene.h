#ifndef SCENE_H
#define SCENE_H

#define GLEW_STATIC
#include "GL/glew.h"
#include "GL/glfw3.h"

class Scene
{
public:
    virtual void processInput() = 0;
    virtual void step() = 0;
    virtual void render() = 0;

    virtual bool switchScene() = 0;
    virtual Scene* nextScene() = 0;
};

#endif // SCENE_H
