#ifndef MODEL_H
#define MODEL_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include <tinyobjloader/tiny_obj_loader.h>

#include <string>
#include <vector>

#include "camera.h"
#include "mesh.h"

class Model
{
public:
    Model(const char* path, GLuint shaderProgram);
    void draw(glm::mat4 model, Camera* camera, Light* light);

private:
    GLuint shaderProgram;
    std::vector<Mesh> meshes;
    std::vector<Texture> texturesLoaded;
    std::string directory;

    void loadModel(std::string path);
    Mesh processShape(tinyobj::shape_t shape, std::vector<tinyobj::material_t> materials);
    Texture loadMaterialTexture(std::string texturePath, std::string type);
};

#endif // MODEL_H
