#ifndef COMPETITOR_H
#define COMPETITOR_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "boundingbox2d.h"
#include "keyboard.h"
#include "model.h"
#include "powerup.h"

typedef struct{
    // Position of the object
    glm::vec3 position;
    // Direction UNITY of the object
    glm::vec3 direction;

    // VelocityModule, the spam vector of the velocity is direction
    GLfloat velocityModule;
    // Angle to rotate the model
    GLfloat angle;

    GLfloat leftTime, rightTime;

} CINEMATICS;

typedef struct{
    GLfloat handling;
    GLfloat braking;
    GLfloat acceleration;
    GLfloat mass;

    // Ex.: in velocity proximate 0 curve x% more than max velocity
    GLfloat velocityCurvature;
    // Base angle to do curves
    GLfloat baseAngle;

    // MAX modules that can be reached
    GLfloat maxVelocity;
    GLfloat maxVelocityBack;

    // %/s decrement
    GLfloat decreVelocity;


} STATS;

class Competitor
{
public:
    Competitor(Model* model);

    bool collided;
    unsigned int ID;

    BoundingBox2D* boundingBox;
    CINEMATICS currentCine, updatedCine, collisionCine;
    STATS kartStats;

    virtual void processAction(std::vector<PowerUp*>* powerUps, bool usePowerUp);
    void render(Camera* camera, Light* light);

    void calculateMovement(bool accelerate, bool brake, bool left, bool right);
    void collide(Competitor* other);
    void collide(BoundingBox2D* obstacle);
    void collide(PowerUp* powerUp);

    bool haveEmptySlot();
    void collectPowerUp();
    int getPowerUpID();
    virtual void finishRace(int position);
    bool finished();

private:
    Model* model;
    PowerUp* powerUp;

    GLfloat turboTime;

    void processMovement(bool accelerate, bool brake, bool left, bool right);
    void processCollision();

    bool finishedRace;
    int racePosition;

    GLfloat leftTime, rightTime; // The time the competitor started turning
    GLfloat computeCurve(GLfloat lastActTime);
};

#endif // COMPETITOR_H
