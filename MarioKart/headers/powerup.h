#ifndef POWERUP_H
#define POWERUP_H

#include "boundingbox2d.h"
#include "model.h"
#include "track.h"

// GREEN_SHELL isn't working, so it was removed
enum powerUpType { MUSHROOM, BANANA_SKIN, last }; // The last is used in the random function to get a random number before the last

class PowerUp
{
public:
    static std::map<powerUpType, Model*> models;
    static void initialize(GLfloat shaderProgram);

    powerUpType type;

    PowerUp();

    bool finished;

    void render(Camera* camera, Light* light);
    powerUpType use(glm::vec3 position, glm::vec3 direction);
    void process(Track* track);
    bool collides(BoundingBox2D* target);
    int getID();

    BoundingBox2D* boundingBox;
private:
    bool used;

    glm::vec3 position, direction;
    GLfloat scale;
};

#endif // POWERUP_H
