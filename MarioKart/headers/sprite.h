#ifndef SPRITE_H
#define SPRITE_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <common/shader.hpp>

#include <stdio.h>
#include <map>
#include <string>

#include "texture.h"

class Sprite
{
public:
    static GLint programShader;
    static std::map<std::string, Sprite*> sprites;

    Sprite(const char* texturePath, bool alpha);
    static void spriteInit(const char *vShaderFile, const char *fShaderFile);
    void drawSprite(glm::vec2 position, glm::vec2 size, GLfloat rotate, glm::vec3 color, glm::mat4 projection);

private:
    Texture2D texture;
    GLuint quadVAO;

    void initRenderData();
    Texture2D loadTextureFromFile(const GLchar *file, GLboolean alpha);
};

#endif // SPRITE_H
