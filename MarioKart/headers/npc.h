#ifndef NPC_H
#define NPC_H

#include "competitor.h"

#include "ai.h"

class NPC : public Competitor
{
public:
    NPC(Model* model, AI* ai);

    void processAI(Track* trackInfo);
    virtual void processAction(std::vector<PowerUp*>* powerUps, bool canMove);

    virtual void finishRace(int position);

private:
    AI *ai;
};

#endif // NPC_H
