#ifndef AI_H
#define AI_H

#include "competitor.h"
#include "track.h"

// Going in which axis and which direction: z or x, positive to negative or negative to positive
enum MovementState { Z_PN, X_PN, Z_NP, X_NP };

class AI
{
public:
    AI();

    bool accelerateAction, brakeAction, leftAction, rightAction, useItemAction;

    void process(Track* trackInfo, Competitor* competitorInfo);

private:
    MovementState currentState;
    GLfloat expectedSpeed;
    glm::vec3 expectedDirection;

    bool collided;
    GLfloat collideTimer;

    void resetStatus();
};

#endif // AI_H
