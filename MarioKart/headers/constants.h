#ifndef CONSTANTS_H
#define CONSTANTS_H

// Paths
#define RESOURCES_PATH  "resources/"
#define MODELS_PATH     RESOURCES_PATH "models/"
#define SPRITES_PATH    RESOURCES_PATH "sprites/"
#define TRACKS_PATH     RESOURCES_PATH "tracks/"
#define CHARACTERS_PATH MODELS_PATH "characters/"

#define DEFAULT_VERTEX_SHADER   RESOURCES_PATH "shaders/simple.vs"
#define DEFAULT_FRAGMENT_SHADER RESOURCES_PATH "shaders/simple.fs"

// Available stuff
#define AVAILABLE_CHARACTERS "mario", "luigi", "bowser", "peach", "wario", "toad", "drybones", "yoshi"
#define AVAILABLE_TRACKS     "simple_track"
#define NUM_POWERUPS         9

// Limits
#define MAX_PLAYERS     4   // Max playable competitors in a game
#define MAX_COMPETITORS 8   // Max competitors per race

// NOT CLASSIFIED
#define ITEMS_ROTATION_SPEED 1.0
#define BOX_SCALE            0.25
#define BOX_SPAWN_TIME       10
#define CHECKPOINTS_DEPTH    2    // The lower this number higher the performance in the bounding box checking (the checkpoint testing function can be optimized for better results)

#define BANANA_SCALE 0.1
#define SHELL_SCALE  0.1
#define TURBO_TIME   1
#define TURBO_SPEED  40

#define PAUSE_KEY GLFW_KEY_ESCAPE

// Temporary
#define NUM_PLAYERS 1
#define AI_ACTIVE   1 // 0 = false | 1 = true

// Definitive
#define PI       3.14159265359
#define BOX_SIZE 5.0

#endif // CONSTANTS_H
