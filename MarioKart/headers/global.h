#ifndef GLOBAL_H
#define GLOBAL_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

class Global
{
public:
    // Window
    static GLFWwindow* window();
    static void setWindow(GLFWwindow* window);
    static void windowSize(int* width, int* height);

    // Delta time
    static GLfloat deltaTime();
    static void updateDeltaTime();

private:
    // Window
    static GLFWwindow* windowVar;

    // Delta time
    static GLfloat deltaTimeVar;
    static GLfloat lastFrame, currentFrame;
};

#endif // GLOBAL_H
