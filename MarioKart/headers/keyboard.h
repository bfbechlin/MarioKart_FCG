#ifndef KEYBOARD_H
#define KEYBOARD_H

#define GLEW_STATIC
#include "GL/glew.h"
#include "GL/glfw3.h"

typedef struct key{
    int code;
    bool pressed;
    bool executed;
}KEY_T;

class Keyboard
{
public:
    Keyboard(int accelerate, int brake, int left, int right, int useItem, int changeCamera);

    // Global keys
    static KEY_T pauseKey;

    // Keys for each action
    KEY_T accelerateKey, brakeKey, leftKey, rightKey;
    KEY_T useItemKey, changeCameraKey;

    void processInput();

private:
    void resetStatus();
};


#endif // KEYBOARD_H
