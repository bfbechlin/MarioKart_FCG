#ifndef MENU_H
#define MENU_H

#define GLEW_STATIC
#include "GL/glew.h"
#include "GL/glfw3.h"

#include "scene.h"

#include "camera.h"
#include "competitor.h"
#include "constants.h"
#include "game.h"
#include "text.h"

enum menuScreen { MAIN_SCREEN, CHAR_SELECT, OPTIONS, RACE_RESULTS };

class Game;
class Menu : public Scene
{
public:
    Menu();
    Menu(std::vector<Competitor*> competitors);

    virtual void processInput();
    virtual void step();
    virtual void render();

    virtual bool switchScene();
    virtual Scene* nextScene();

private:
    bool shouldSwitch;

    GLuint defaultShader;
    Track* gameTrack;

    std::string characters[8];
    Competitor* competitors[MAX_COMPETITORS];
    int numPlayers;
    int selectedPlayers;
    Keyboard* plControls[4];

    int selection;
    Keyboard* keyboard;

    Text* text;
    menuScreen screen;
    Camera* camera;

    Game* gameScene;


    // CHAR_SELECT HUD elements
    Hud* charsHud;


    void initVars();
};

#endif // MENU_H
