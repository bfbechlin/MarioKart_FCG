#ifndef CAMERA_H
#define CAMERA_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>

enum CameraMode {FIRST_PERSON, THIRD_PERSON_1, THIRD_PERSON_2, THIRD_PERSON_3, SPECTATOR};

class Camera
{
public:
    glm::mat4 view, projection;
    float x, y, width, height;
    glm::vec3 position;

    Camera(float x, float y, float width, float height);

    void setViewport(bool perspective);
    void updatePosition(glm::vec3* playerPosition, glm::vec3* direction);
    void circleViewModes();
    void setToSpectator();

private:
    float zoom;
    glm::vec3 upVec, positionOffset, centerOffset;
    GLfloat rotationDelayFactor, speedFactor;

    GLfloat angle;
    bool staticCam;

    CameraMode currentMode;

    void updateModeProperties();
};

#endif // CAMERA_H
