#ifndef TRACK_H
#define TRACK_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include "boundingbox2d.h"
#include "box.h"
#include "camera.h"
#include "model.h"

#include <string>
#include <vector>

typedef struct competitorCheckStatus
{
    int checkpoint;
    GLfloat checkpointTime;
} COMP_CHECK;

class Track
{
public:
    std::vector<glm::vec3> playersStartingPositions;
    std::vector<glm::vec3> playersStartingDirections;
    std::vector<BoundingBox2D*> walls;
    Light* light;
    GLfloat scale;
    int numLaps;

    Track(const char* path, GLuint shaderProgram);

    void render(Camera* camera);

    void update();

    void generatePlayersIDs(int quantity);
    void verifyCheckpoints(unsigned int competitorID, BoundingBox2D* competitorBoundingBox);
    bool verifyCollectibles(BoundingBox2D* competitorBoundingBox);

    int getLap(unsigned int competitorID);
    int getPosition(unsigned int competitorID);

private:
    Model* model;

    std::vector<COMP_CHECK> competitorStatus;
    std::vector<Box*> collectibles;
    std::vector<BoundingBox2D*> checkpoints;

    void loadTrack(const char* path, GLuint shaderProgram);
};

#endif // TRACK_H
