/*
    Using cartesian coordenates, but the y coordenate is always 0.
    Remenber that it's using homogeneous coordenates so [x, y, z, w]
    Where w is 1 or other non zero value when its a POINT
    Where w is 0 its a VECTOR
*/
#ifndef BOUNDINGBOX2D_H
#define BOUNDINGBOX2D_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include <glm/glm.hpp>

class BoundingBox2D
{
public:
    //Constructor
    BoundingBox2D(glm::vec3 center, const GLdouble w, const GLdouble h, glm::vec3 direction);

    void movement(glm::vec3 newPosition, glm::vec3 newDirection);
    void copy(BoundingBox2D* source);

    glm::vec3 getCenter();
    glm::vec3 getDirection();
    void printData();

    bool overlaps(BoundingBox2D* other);
    bool isInside(BoundingBox2D* other);

private:
    // Vertices of the bounding box
    glm::vec3 vertex[4];
    // Center of bounding box
    glm::vec3 center;
    // Direction of moviment
    glm::vec3 direction;
    // Two edges extende away from corner[0]
    glm::vec3 axis[2];
    // origin[a] = corner[0].dot(axis[a]);
    GLdouble origin[2];
    // Matrix to found the vertices using the orientation
    glm::mat4 model[4];

    bool overlaps1Way(BoundingBox2D* other, bool inside);
    void computeAxis();
    void computeVertex();
    void computeMatrixModel();
};

#endif // BOUNDINGBOX2D_H
