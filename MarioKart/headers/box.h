#ifndef BOX_H
#define BOX_H

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "boundingbox2d.h"
#include "model.h"

class Box
{
public:
    static GLfloat rotation;

    BoundingBox2D* boundingBox;

    Box(glm::vec3 position, GLuint shaderProgram);
    void update(GLfloat deltaTime);
    void render(Camera* camera, Light* light);

    void collect();
    bool collected();

private:
    Model* model;
    GLfloat spawnCounter;
    glm::vec3 position;
};

#endif // BOX_H
