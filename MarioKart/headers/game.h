#ifndef GAME_H
#define GAME_H

#define GLEW_STATIC
#include "GL/glew.h"
#include "GL/glfw3.h"

#include "scene.h"

#include <vector>

#include "hud.h"
#include "menu.h"
#include "npc.h"
#include "player.h"
#include "track.h"

class Menu;
class Game : public Scene
{
public:
    Game(Track* level, Competitor** competitors, int numCompetitors);

    virtual void processInput();
    virtual void step();
    virtual void render();

    virtual bool switchScene();
    virtual Scene* nextScene();

    std::vector<Hud*> hudLayout;

private:
    bool shouldSwitch;
    GLfloat delayToBegin;
    GLfloat timeInicialGame = 0;

    Track* track;
    Hud* minimap;
    Camera* globalCamera;

    // Both the vector bellow have the same data
    // These are for when it's necessary to iterate over players and NPCs separately
    std::vector<Player*> players;
    std::vector<NPC*>    npcs;
    // This is for when it's necessary to iterate over all competitors
    std::vector<Competitor*> competitors;

    // Vector for power ups on the game
    std::vector<PowerUp*> powerUps;

    int finishPosition; // Finish position counter

    Menu* menuScene;

    void changeToResultsScreen();
};

#endif // GAME_H
